/**
 * Guice-related information (i.e., annotations and modules relevant for the DPWS implementation).
 */
@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.dpws.guice;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
