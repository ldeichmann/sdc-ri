package org.somda.sdc.dpws.http

import java.util.UUID
import javax.net.ssl.SSLSession

/**
 * Http connection interceptor interface.
 */
interface HttpConnectionInterceptor {
    /**
     * The source that established the connection.
     */
    enum class Source {
        CLIENT,
        SERVER
    }

    /**
     * Called once, when the first message is sent through a connection.
     *
     * @param connectionId the unique id of the connection
     * @param sslSession the SSL session of the connection
     * @param source the source, that established the connection
     */
    fun onFirstIntercept(connectionId: UUID, sslSession: SSLSession, source: Source) {
        // Do nothing by default
    }

    /**
     * Called on every message sent through a connection.
     *
     * @param connectionId the unique id of the connection
     */
    fun onIntercept(connectionId: UUID) {
        // Do nothing by default
    }

    /**
     * Called once, when the last message is sent through a connection.
     *
     * @param connectionId the unique id of the connection
     */
    fun onLastIntercept(connectionId: UUID) {
        // Do nothing by default
    }
}
