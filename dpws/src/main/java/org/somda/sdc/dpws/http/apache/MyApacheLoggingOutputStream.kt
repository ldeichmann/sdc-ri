package org.somda.sdc.dpws.http.apache

import org.apache.http.impl.conn.Wire
import java.io.IOException
import java.io.OutputStream
import java.util.*

/**
 * This class is a Kotlin conversion of [org.apache.http.impl.conn.LoggingOutputStream].
 */
internal class MyApacheLoggingOutputStream(private val out: OutputStream, private val wire: Wire) : OutputStream() {
    @Throws(IOException::class)
    override fun write(b: Int) {
        try {
            wire.output(b)
        } catch (ex: IOException) {
            wire.output("[write] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun write(b: ByteArray) {
        try {
            wire.output(b)
            out.write(b)
        } catch (ex: IOException) {
            wire.output("[write] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun write(b: ByteArray, off: Int, len: Int) {
        try {
            wire.output(b, off, len)
            out.write(b, off, len)
        } catch (ex: IOException) {
            wire.output("[write] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun flush() {
        try {
            out.flush()
        } catch (ex: IOException) {
            wire.output("[flush] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun close() {
        try {
            out.close()
        } catch (ex: IOException) {
            wire.output("[close] I/O error: " + ex.message)
            throw ex
        }
    }
}
