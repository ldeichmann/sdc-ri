@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package test.org.somda.common;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;