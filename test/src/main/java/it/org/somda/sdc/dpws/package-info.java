@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package it.org.somda.sdc.dpws;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;