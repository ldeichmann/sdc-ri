package org.somda.sdc.glue.provider.sco

import org.somda.sdc.biceps.model.message.Activate
import org.somda.sdc.biceps.model.message.InvocationError
import org.somda.sdc.biceps.model.message.InvocationState
import org.somda.sdc.biceps.model.message.SetAlertState
import org.somda.sdc.biceps.model.message.SetComponentState
import org.somda.sdc.biceps.model.message.SetContextState
import org.somda.sdc.biceps.model.message.SetMetricState
import org.somda.sdc.biceps.model.message.SetString
import org.somda.sdc.biceps.model.message.SetValue
import org.somda.sdc.biceps.model.participant.LocalizedText
import java.lang.Exception

/**
 * Class that is capable of processing incoming set service requests.
 */
interface OperationInvocationReceiver {

    @Throws(Exception::class)
    fun handleSetValue(context: Context, operationHandle: String, setValue: SetValue): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    @Throws(Exception::class)
    fun handleSetString(context: Context, operationHandle: String, setString: SetString): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    @Throws(Exception::class)
    fun handleActivate(context: Context, operationHandle: String, activate: Activate): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    @Throws(Exception::class)
    fun handleSetAlertState(
        context: Context,
        operationHandle: String,
        setAlertState: SetAlertState
    ): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    @Throws(Exception::class)
    fun handleSetMetricState(
        context: Context,
        operationHandle: String,
        setMetricState: SetMetricState
    ): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    @Throws(Exception::class)
    fun handleSetContextState(
        context: Context,
        operationHandle: String,
        setContextState: SetContextState
    ): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    @Throws(Exception::class)
    fun handleSetComponentState(
        context: Context,
        operationHandle: String,
        setComponentState: SetComponentState
    ): InvocationResponse {
        return additionallySendResponseAsReport(context, context.createUnsuccessfulResponse(
            InvocationState.FAIL,
            InvocationError.OTH,
            listOf(createLocalizedText(MISSING_HANDLER.format(operationHandle)))
        ))
    }

    companion object {
        internal const val MISSING_HANDLER = "A handler for the operation with handle '%s' could not be found"

        internal fun createLocalizedText(textValue: String): LocalizedText {
            return LocalizedText().apply {
                value = textValue
                lang = "en"
            }
        }

        internal fun additionallySendResponseAsReport(context: Context, response: InvocationResponse): InvocationResponse {
            context.sendReport(
                response.mdibVersion,
                response.invocationState,
                response.invocationError,
                response.invocationErrorMessage,
                null
            )
            return response
        }
    }
}