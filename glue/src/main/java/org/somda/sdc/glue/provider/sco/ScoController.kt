package org.somda.sdc.glue.provider.sco

import com.google.inject.assistedinject.Assisted
import com.google.inject.assistedinject.AssistedInject
import com.google.inject.name.Named
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.somda.sdc.biceps.model.message.Activate
import org.somda.sdc.biceps.model.message.InvocationError
import org.somda.sdc.biceps.model.message.InvocationState
import org.somda.sdc.biceps.model.message.SetAlertState
import org.somda.sdc.biceps.model.message.SetComponentState
import org.somda.sdc.biceps.model.message.SetContextState
import org.somda.sdc.biceps.model.message.SetMetricState
import org.somda.sdc.biceps.model.message.SetString
import org.somda.sdc.biceps.model.message.SetValue
import org.somda.sdc.biceps.model.participant.InstanceIdentifier
import org.somda.sdc.biceps.provider.access.LocalMdibAccess
import org.somda.sdc.common.CommonConfig
import org.somda.sdc.common.logging.InstanceLogger
import org.somda.sdc.dpws.device.EventSourceAccess
import org.somda.sdc.glue.provider.sco.OperationInvocationReceiver.Companion.MISSING_HANDLER
import org.somda.sdc.glue.provider.sco.OperationInvocationReceiver.Companion.createLocalizedText
import org.somda.sdc.glue.provider.sco.factory.ContextFactory
import jakarta.annotation.Nullable;

/**
 * Manages callbacks for incoming set service requests.
 */
class ScoController @AssistedInject constructor(
    @Assisted private val eventSourceAccess: EventSourceAccess,
    @Assisted private val mdibAccess: LocalMdibAccess,
    private val contextFactory: ContextFactory,
    @Named(CommonConfig.INSTANCE_IDENTIFIER) private val frameworkIdentifier: String,
    @Assisted @Nullable
    private val operationInvocationReceiver: OperationInvocationReceiver?
) {
    private var transactionCounter: Long = 0
    private var instanceLogger: Logger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier)

    companion object {
        val LOG: Logger = LogManager.getLogger(ScoController::class)
    }

    /**
     * Invokes processing of an incoming network set service call.
     *
     * @param handle  the handle of the operation that was called.
     * @param source  the instance identifier that represents the calling client.
     * @param request the request data.
     * @param <T>     type of the request data.
     * @param callerId string that uniquely identifies the certificate of the calling consumer (e.g. Distinguished Name)
     * @throws RuntimeException if the handle of the operation is not known and no source mds can be determined
     * @return the initial invocation info required for the response message. The initial invocation info is
     * requested by the callback that is going to be invoked. In case that no callback can be found, a fail state is
     * returned.
     */
    fun <T> processIncomingSetOperation(
        handle: String,
        source: InstanceIdentifier,
        request: T,
        callerId: String?
    ): InvocationResponse {
        val context: Context =
            contextFactory.createContext(transactionCounter++, handle, source, eventSourceAccess, mdibAccess, callerId)

        try {
            val response = when (request) {
                is SetValue ->  {
                    operationInvocationReceiver?.handleSetValue(context, handle, request as SetValue)
                }

                is SetString -> {
                    operationInvocationReceiver?.handleSetString(context, handle, request as SetString)
                }

                is Activate -> {
                    operationInvocationReceiver?.handleActivate(context, handle, request as Activate)
                }

                is SetAlertState -> {
                    operationInvocationReceiver?.handleSetAlertState(context, handle, request as SetAlertState)
                }

                is SetComponentState -> {
                    operationInvocationReceiver?.handleSetComponentState(context, handle, request as SetComponentState)
                }

                is SetContextState -> {
                    operationInvocationReceiver?.handleSetContextState(context, handle, request as SetContextState)
                }

                is SetMetricState -> {
                    operationInvocationReceiver?.handleSetMetricState(context, handle, request as SetMetricState)
                }

                else -> null
            }
            response?.let {
                return it
            }

        } catch (e: Exception) {
            val errorMessage = "Invocation of operation with handle '$handle' failed: ${e.message}"
            instanceLogger.warn(errorMessage)
            instanceLogger.trace(errorMessage, e)

            return OperationInvocationReceiver.additionallySendResponseAsReport(
                context, context.createUnsuccessfulResponse(
                    mdibAccess.mdibVersion,
                    InvocationState.FAIL,
                    InvocationError.OTH,
                    listOf(createLocalizedText(errorMessage))
                )
            )
        }
        return OperationInvocationReceiver.additionallySendResponseAsReport(
            context, context.createUnsuccessfulResponse(
                mdibAccess.getMdibVersion(),
                InvocationState.FAIL,
                InvocationError.OTH,
                listOf(createLocalizedText(MISSING_HANDLER.format(handle)))
            )
        )
    }
}