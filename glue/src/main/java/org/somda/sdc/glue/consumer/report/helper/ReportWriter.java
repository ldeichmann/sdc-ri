package org.somda.sdc.glue.consumer.report.helper;

import com.google.inject.Inject;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibDescriptionModificationType;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibStateModifications;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.PairException;
import org.somda.sdc.biceps.common.storage.PreprocessingException;
import org.somda.sdc.biceps.consumer.access.RemoteMdibAccess;
import org.somda.sdc.biceps.model.message.AbstractAlertReport;
import org.somda.sdc.biceps.model.message.AbstractComponentReport;
import org.somda.sdc.biceps.model.message.AbstractContextReport;
import org.somda.sdc.biceps.model.message.AbstractMetricReport;
import org.somda.sdc.biceps.model.message.AbstractOperationalStateReport;
import org.somda.sdc.biceps.model.message.AbstractReport;
import org.somda.sdc.biceps.model.message.DescriptionModificationReport;
import org.somda.sdc.biceps.model.message.DescriptionModificationType;
import org.somda.sdc.biceps.model.message.WaveformStream;
import org.somda.sdc.biceps.model.participant.AbstractAlertState;
import org.somda.sdc.biceps.model.participant.AbstractContextDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractContextState;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractDeviceComponentState;
import org.somda.sdc.biceps.model.participant.AbstractMetricState;
import org.somda.sdc.biceps.model.participant.AbstractOperationState;
import org.somda.sdc.biceps.model.participant.AbstractState;
import org.somda.sdc.biceps.model.participant.MdibVersion;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.glue.common.MdibVersionUtil;
import org.somda.sdc.glue.consumer.report.ReportProcessingException;

import jakarta.annotation.Nullable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Helper class that accepts any state reports and writes them to a {@linkplain RemoteMdibAccess} instance.
 * <p>
 * The {@linkplain ReportWriter} acts as a dispatcher for all episodic reports SDC generates and which have to be transformed
 * to modifications a {@link RemoteMdibAccess} instance understands.
 */
public class ReportWriter {
    private final MdibVersionUtil mdibVersionUtil;

    @Inject
    ReportWriter(MdibVersionUtil mdibVersionUtil) {
        this.mdibVersionUtil = mdibVersionUtil;
    }

    /**
     * Transforms the given report to a modifications set and writes it to the {@linkplain RemoteMdibAccess} instance.
     *
     * @param report     the report to write.
     * @param mdibAccess the MDIB access to write to.
     * @throws ReportProcessingException in case the report cannot be transformed or dispatched correctly.
     * @throws PreprocessingException    corresponds to the exception that {@link RemoteMdibAccess#writeDescription(
     *                                   MdibVersion, BigInteger, BigInteger, MdibDescriptionModifications)} or
     *                                   {@link RemoteMdibAccess#writeStates(MdibVersion, MdibStateModifications)}
     *                                   throws.
     */
    public void write(EpisodicReport report, RemoteMdibAccess mdibAccess)
            throws ReportProcessingException, PreprocessingException {
        if (report instanceof EpisodicReport.Waveform) {
            final var rep = (EpisodicReport.Waveform) report;
            write(rep.getReport(), makeModifications(rep.getReport()), mdibAccess);
        } else if (report instanceof EpisodicReport.Metric) {
            final var rep = (EpisodicReport.Metric) report;
            write(rep.getReport(), makeModifications(rep.getReport()), mdibAccess);
        } else if (report instanceof EpisodicReport.Alert) {
            final var rep = (EpisodicReport.Alert) report;
            write(rep.getReport(), makeModifications(rep.getReport()), mdibAccess);
        } else if (report instanceof EpisodicReport.Operation) {
            final var rep = (EpisodicReport.Operation) report;
            write(rep.getReport(), makeModifications(rep.getReport()), mdibAccess);
        } else if (report instanceof EpisodicReport.Component) {
            final var rep = (EpisodicReport.Component) report;
            write(rep.getReport(), makeModifications(rep.getReport()), mdibAccess);
        } else if (report instanceof EpisodicReport.Context) {
            final var rep = (EpisodicReport.Context) report;
            write(rep.getReport(), makeModifications(rep.getReport()), mdibAccess);
        } else if (report instanceof EpisodicReport.Description) {
            final var rep = (EpisodicReport.Description) report;
            write(rep.getReport(), mdibAccess);
        } else {
            throw new ReportProcessingException(String.format("Unexpected report type: %s",
                    report.getClass().getSimpleName()));
        }
    }

    private void write(AbstractReport report, MdibStateModifications modifications, RemoteMdibAccess mdibAccess)
            throws PreprocessingException {
        mdibAccess.writeStates(mdibVersionUtil.getMdibVersion(report), modifications);
    }

    private void write(DescriptionModificationReport report, RemoteMdibAccess mdibAccess)
            throws ReportProcessingException, PreprocessingException {
        mdibAccess.writeDescription(mdibVersionUtil.getMdibVersion(report),
                null,
                null,
                mdibDescriptionModifications(report));
    }

    private MdibDescriptionModifications mdibDescriptionModifications(DescriptionModificationReport report)
            throws ReportProcessingException {
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications();

        final List<MdibDescriptionModification> partModifications = new ArrayList<>();
        for (var reportPart: report.getReportPart()) {
            final MdibDescriptionModificationType modType = mapModType(reportPart.getModificationType());

            final Optional<String> parentDescriptor = Optional.ofNullable(reportPart.getParentDescriptor());

            final Map<String, List<AbstractState>> stateMap = new HashMap<>();

            for (var state: reportPart.getState()) {
                var descriptorHandle = state.getDescriptorHandle();
                stateMap.computeIfAbsent(descriptorHandle, k -> new ArrayList<>()).add(state);
            }

            final var descriptorHandles = reportPart.getDescriptor().stream().map(AbstractDescriptor::getHandle).collect(Collectors.toSet());

            final var unknownDescriptors = stateMap.keySet().stream()
                .filter(abstractStates -> !descriptorHandles.contains(abstractStates))
                .collect(Collectors.toList());

            if (!unknownDescriptors.isEmpty()) {
                throw new ReportProcessingException(String.format("State(s) %s had no matching descriptor in report", String.join(", ", unknownDescriptors)));
            }

            final List<MdibDescriptionModification> descriptionModifications = new ArrayList<>();

            for (final var descriptor: reportPart.getDescriptor()) {
                final var statesOpt = Optional.ofNullable(stateMap.remove(descriptor.getHandle()));

                if (descriptor instanceof AbstractContextDescriptor) {

                    List<AbstractContextState> contextStates = new ArrayList<>();
                    if (statesOpt.isPresent()) {
                        final var states = statesOpt.orElseThrow();
                        for (final var state: states) {
                            if (!(state instanceof AbstractContextState)) {
                                throw new ReportProcessingException("MultiState descriptor with single state type");
                            } else {
                                contextStates.add((AbstractContextState) state);
                            }
                        }
                    }

                    if (modType == MdibDescriptionModificationType.INSERT) {
                        try {
                            descriptionModifications.add(new MdibDescriptionModification.Insert(
                                Pair.tryFromThrowing(descriptor, contextStates),
                                parentDescriptor.orElseThrow(() -> new ReportProcessingException(String.format("MultiState %s with unknown parent descriptor", descriptor.getHandle())))
                            ));
                        } catch (PairException e) {
                            throw new ReportProcessingException(e);
                        }
                    } else if (modType == MdibDescriptionModificationType.UPDATE) {
                        try {
                            descriptionModifications.add(new MdibDescriptionModification.Update(
                                    Pair.tryFromThrowing(descriptor, contextStates)
                            ));
                        } catch (PairException e) {
                            throw new ReportProcessingException(e);
                        }
                    } else {
                        descriptionModifications.add(new MdibDescriptionModification.Delete(descriptor.getHandle()));
                    }
                } else {
                    if (modType == MdibDescriptionModificationType.INSERT) {
                        if (statesOpt.isEmpty()) {
                            throw new ReportProcessingException(String.format("No states present for descriptor %s", descriptor.getHandle()));
                        }
                        final var states = statesOpt.orElseThrow();
                        if (states.size() != 1) {
                            throw new ReportProcessingException(String.format("Descriptor %s only allows single state, found %s", descriptor.getHandle(), states.size()));
                        }
                        final var state = states.remove(0);

                        String parentDesc = null;
                        if (!(descriptor instanceof MdsDescriptor)) {
                            parentDesc = parentDescriptor.orElseThrow(
                                () -> new ReportProcessingException(String.format(
                                    "SingleState %s with unknown parent descriptor that is not Mds",
                                    descriptor.getHandle()
                                )));
                        }

                        try {
                            descriptionModifications.add(new MdibDescriptionModification.Insert(
                                Pair.tryFromThrowing(descriptor, state),
                                parentDesc
                            ));
                        } catch (PairException e) {
                            throw new ReportProcessingException(e);
                        }
                    } else if (modType == MdibDescriptionModificationType.UPDATE) {
                        if (statesOpt.isEmpty()) {
                            throw new ReportProcessingException(String.format("No states present for descriptor %s", descriptor.getHandle()));
                        }
                        final var states = statesOpt.orElseThrow();
                        if (states.size() != 1) {
                            throw new ReportProcessingException(String.format("Descriptor %s only allows single state, found %s", descriptor.getHandle(), states.size()));
                        }
                        final var state = states.remove(0);
                        try {
                            descriptionModifications.add(new MdibDescriptionModification.Update(
                                Pair.tryFromThrowing(descriptor, state)
                            ));
                        } catch (PairException e) {
                            throw new ReportProcessingException(e);
                        }
                    } else {
                        descriptionModifications.add(new MdibDescriptionModification.Delete(descriptor.getHandle()));
                    }
                }
            }

            partModifications.addAll(descriptionModifications);
        }

        try {
            modifications.addAll(partModifications);
        } catch (RuntimeException e) {
            throw new ReportProcessingException(e);
        }
        return modifications;
    }

    private MdibStateModifications makeModifications(WaveformStream report) {
        return new MdibStateModifications.Waveform(report.getState());
    }

    private MdibStateModifications makeModifications(AbstractMetricReport report) {
        final int capacity = report.getReportPart().stream().mapToInt(rp -> rp.getMetricState().size()).sum();
        final var states = new ArrayList<AbstractMetricState>(capacity);

        for (AbstractMetricReport.ReportPart reportPart : report.getReportPart()) {
            states.addAll(reportPart.getMetricState());
        }

        return new MdibStateModifications.Metric(states);
    }

    private MdibStateModifications makeModifications(AbstractAlertReport report) {
        final int capacity = report.getReportPart().stream().mapToInt(rp -> rp.getAlertState().size()).sum();
        final var states = new ArrayList<AbstractAlertState>(capacity);

        for (AbstractAlertReport.ReportPart reportPart : report.getReportPart()) {
            states.addAll(reportPart.getAlertState());
        }

        return new MdibStateModifications.Alert(states);
    }

    private MdibStateModifications makeModifications(AbstractComponentReport report) {
        final int capacity = report.getReportPart().stream().mapToInt(rp -> rp.getComponentState().size()).sum();
        final var states = new ArrayList<AbstractDeviceComponentState>(capacity);

        for (AbstractComponentReport.ReportPart reportPart : report.getReportPart()) {
            states.addAll(reportPart.getComponentState());
        }

        return new MdibStateModifications.Component(states);
    }

    private MdibStateModifications makeModifications(AbstractContextReport report) {
        final int capacity = report.getReportPart().stream().mapToInt(rp -> rp.getContextState().size()).sum();
        final var states = new ArrayList<AbstractContextState>(capacity);

        for (AbstractContextReport.ReportPart reportPart : report.getReportPart()) {
            states.addAll(reportPart.getContextState());
        }

        return new MdibStateModifications.Context(states);
    }

    private MdibStateModifications makeModifications(AbstractOperationalStateReport report) {
        final int capacity = report.getReportPart().stream().mapToInt(rp -> rp.getOperationState().size()).sum();
        final var states = new ArrayList<AbstractOperationState>(capacity);

        for (AbstractOperationalStateReport.ReportPart reportPart : report.getReportPart()) {
            states.addAll(reportPart.getOperationState());
        }

        return new MdibStateModifications.Operation(states);
    }

    private static MdibDescriptionModificationType mapModType(@Nullable DescriptionModificationType modificationType) {
        if (modificationType == null) {
            return MdibDescriptionModificationType.UPDATE;
        }

        switch (modificationType) {
            case CRT:
                return MdibDescriptionModificationType.INSERT;
            case UPT:
                return MdibDescriptionModificationType.UPDATE;
            case DEL:
                return MdibDescriptionModificationType.DELETE;
            default:
                throw new RuntimeException(String.format("Unexpected description modification type detected. " +
                                "Processing branch is missing: %s",
                        modificationType));
        }
    }
}
