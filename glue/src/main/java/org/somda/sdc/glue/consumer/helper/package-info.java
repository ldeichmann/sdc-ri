/**
 * Helper classes for the Glue consumer side.
 */
@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.glue.consumer.helper;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
