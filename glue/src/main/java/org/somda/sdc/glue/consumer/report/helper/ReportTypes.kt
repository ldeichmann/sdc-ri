package org.somda.sdc.glue.consumer.report.helper

import org.somda.sdc.biceps.model.message.AbstractReport
import org.somda.sdc.biceps.model.message.DescriptionModificationReport
import org.somda.sdc.biceps.model.message.EpisodicAlertReport
import org.somda.sdc.biceps.model.message.EpisodicComponentReport
import org.somda.sdc.biceps.model.message.EpisodicContextReport
import org.somda.sdc.biceps.model.message.EpisodicMetricReport
import org.somda.sdc.biceps.model.message.EpisodicOperationalStateReport
import org.somda.sdc.biceps.model.message.WaveformStream

/**
 * Describes the set of episodic reports which contain changes relevant for the mdib.
 */
sealed class EpisodicReport {
    data class Alert(val report: EpisodicAlertReport): EpisodicReport()
    data class Component(val report: EpisodicComponentReport): EpisodicReport()
    data class Context(val report: EpisodicContextReport): EpisodicReport()
    data class Metric(val report: EpisodicMetricReport): EpisodicReport()
    data class Operation(val report: EpisodicOperationalStateReport): EpisodicReport()
    data class Waveform(val report: WaveformStream): EpisodicReport()
    data class Description(val report: DescriptionModificationReport): EpisodicReport()
    val abstractReport: AbstractReport
        get() = when(this) {
            is Alert -> report
            is Component -> report
            is Context -> report
            is Metric -> report
            is Operation -> report
            is Waveform -> report
            is Description -> report
        }

    companion object {
        @JvmStatic
        fun tryFrom(report: AbstractReport): EpisodicReport? = when (report) {
            is EpisodicAlertReport -> Alert(report)
            is EpisodicComponentReport -> Component(report)
            is EpisodicContextReport -> Context(report)
            is EpisodicMetricReport -> Metric(report)
            is EpisodicOperationalStateReport -> Operation(report)
            is WaveformStream -> Waveform(report)
            is DescriptionModificationReport -> Description(report)
            else -> null
        }

        @JvmStatic
        fun from(report: EpisodicAlertReport) = Alert(report)
        @JvmStatic
        fun from(report: EpisodicComponentReport) = Component(report)
        @JvmStatic
        fun from(report: EpisodicContextReport) = Context(report)
        @JvmStatic
        fun from(report: EpisodicMetricReport) = Metric(report)
        @JvmStatic
        fun from(report: EpisodicOperationalStateReport) = Operation(report)
        @JvmStatic
        fun from(report: WaveformStream) = Waveform(report)
        @JvmStatic
        fun from(report: DescriptionModificationReport) = Description(report)
    }
}