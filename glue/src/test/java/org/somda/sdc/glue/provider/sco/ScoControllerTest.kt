package org.somda.sdc.glue.provider.sco

import org.bouncycastle.util.Integers
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.somda.sdc.biceps.common.MdibTypeValidator
import org.somda.sdc.biceps.model.message.AbstractSet
import org.somda.sdc.biceps.model.message.Activate
import org.somda.sdc.biceps.model.message.InvocationError
import org.somda.sdc.biceps.model.message.InvocationState
import org.somda.sdc.biceps.model.message.OperationInvokedReport
import org.somda.sdc.biceps.model.message.SetAlertState
import org.somda.sdc.biceps.model.message.SetComponentState
import org.somda.sdc.biceps.model.message.SetContextState
import org.somda.sdc.biceps.model.message.SetMetricState
import org.somda.sdc.biceps.model.message.SetString
import org.somda.sdc.biceps.model.message.SetValue
import org.somda.sdc.biceps.model.participant.AbstractAlertState
import org.somda.sdc.biceps.model.participant.AbstractContextState
import org.somda.sdc.biceps.model.participant.AbstractDeviceComponentState
import org.somda.sdc.biceps.model.participant.AbstractMetricState
import org.somda.sdc.biceps.model.participant.AlertActivation
import org.somda.sdc.biceps.model.participant.InstanceIdentifier
import org.somda.sdc.biceps.model.participant.MdibVersion
import org.somda.sdc.biceps.provider.access.factory.LocalMdibAccessFactory
import org.somda.sdc.biceps.testutil.BaseTreeModificationsSet
import org.somda.sdc.biceps.testutil.Handles
import org.somda.sdc.biceps.testutil.MockEntryFactory
import org.somda.sdc.dpws.device.EventSourceAccess
import org.somda.sdc.glue.UnitTestUtil
import org.somda.sdc.glue.common.ActionConstants
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_ACTIVATE_VALUE
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_SET_ALERT_STATE
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_SET_COMPONENT_STATE
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_SET_CONTEXT_STATE
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_SET_METRIC_STATE
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_SET_STRING
import org.somda.sdc.glue.provider.sco.ScoControllerTest.Receiver.IDS.ID_SET_VALUE
import org.somda.sdc.glue.provider.sco.factory.ScoControllerFactory
import test.org.somda.common.LoggingTestWatcher
import java.math.BigDecimal

@ExtendWith(LoggingTestWatcher::class)
class ScoControllerTest {

    private val IT = UnitTestUtil()

    private lateinit var scoController: ScoController
    private lateinit var eventSourceAccessMock: EventSourceAccess
    private lateinit var receiver: Receiver
    private lateinit var actionCaptor: ArgumentCaptor<String>
    private lateinit var reportCaptor: ArgumentCaptor<OperationInvokedReport>

    @BeforeEach
    @Throws(Exception::class)
    fun beforeEach() {
        eventSourceAccessMock = mock(EventSourceAccess::class.java)
        actionCaptor = ArgumentCaptor.forClass(String::class.java)
        reportCaptor = ArgumentCaptor.forClass(OperationInvokedReport::class.java)

        val modifications =
            BaseTreeModificationsSet(MockEntryFactory(IT.injector.getInstance(MdibTypeValidator::class.java)))

        val localMdibAccess = IT.injector.getInstance(LocalMdibAccessFactory::class.java).createLocalMdibAccess()
        localMdibAccess.writeDescription(modifications.createBaseTree())

        receiver = Receiver
        scoController = IT.injector.getInstance(ScoControllerFactory::class.java)
            .createScoController(eventSourceAccessMock, localMdibAccess, Receiver as OperationInvocationReceiver)
    }

    @Test
    fun `test invocation receiver is processed correctly`() {
        val instanceIdentifier = InstanceIdentifier().apply {
            rootName = "http://example.com/toor"
            extensionName = "foo"
        }
        // test SetValueOperation
        var operation = Handles.OPERATION_2
        var callbackId = ID_SET_VALUE
        var itemSize = 1
        var itemIndex = 0
        var transactionId = 0L

        run {
            val expectedPayload = SetValue().apply {
                requestedNumericValue = BigDecimal.TEN
            }
            val callerId = "cn=Bastian Beispielmensch, o=Stiftung Beispielhaft, c=DE;"
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                callerId
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
            assertEquals(callerId, receiver.items[itemIndex].context.callerId)
        }
        // test ActivateOperation
        operation = Handles.OPERATION_0
        callbackId = ID_ACTIVATE_VALUE
        itemSize++
        itemIndex++
        transactionId++
        run {
            val arg1 = "Test"
            val arg2 = Integers.valueOf(100)
            val expectedPayload = Activate().apply {
                argument = listOf(
                    Activate.Argument().apply {
                        argValue = arg1
                    },
                    Activate.Argument().apply {
                        argValue = arg2
                    }
                )
            }
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
        }
        // test SetStringOperation
        operation = Handles.OPERATION_1
        callbackId = ID_SET_STRING
        itemSize++
        itemIndex++
        transactionId++
        run {
            val stringValue = "newStringValue"
            val expectedPayload = SetString().apply {
                requestedStringValue = stringValue
            }
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
        }
        // test SetComponentStateOperation
        operation = Handles.OPERATION_3
        callbackId = ID_SET_COMPONENT_STATE
        itemSize++
        itemIndex++
        transactionId++
        run {
            val expectedPayload = SetComponentState().apply {
                proposedComponentState = listOf(AbstractDeviceComponentState().apply {
                    descriptorHandle = "newDeviceComponent"
                })
            }
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
        }
        // test SetMetricStateOperation
        operation = Handles.OPERATION_4
        callbackId = ID_SET_METRIC_STATE
        itemSize++
        itemIndex++
        transactionId++
        run {
            val expectedPayload = SetMetricState().apply {
                proposedMetricState = listOf(AbstractMetricState().apply {
                    descriptorHandle = "newMetric"
                })
            }
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
        }
        // test SetAlertStateOperation
        operation = Handles.OPERATION_5
        callbackId = ID_SET_ALERT_STATE
        itemSize++
        itemIndex++
        transactionId++
        run {
            val expectedPayload = SetAlertState().apply {
                proposedAlertState = AbstractAlertState().apply {
                    descriptorHandle = "newAlertState"
                    activationState = AlertActivation.ON
                }
            }
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
        }
        // test SetContextStateOperation
        operation = Handles.OPERATION_6
        callbackId = ID_SET_CONTEXT_STATE
        itemSize++
        itemIndex++
        transactionId++
        run {
            val expectedPayload = SetContextState().apply {
                proposedContextState = listOf(AbstractContextState().apply {
                    descriptorHandle = "newContext"
                    handle = "newContextState"
                })
            }
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )

            assertEquals(itemSize, receiver.items.size)
            assertEquals(callbackId, receiver.items[itemIndex].callbackId)
            assertEquals(instanceIdentifier, receiver.items[itemIndex].context.invocationSource)
            assertEquals(operation, receiver.items[itemIndex].context.operationHandle)
            assertEquals(transactionId, receiver.items[itemIndex].context.transactionId)
            assertEquals(expectedPayload, receiver.items[itemIndex].data)
        }
        // assert each operation was called
        assertEquals(itemSize, Receiver.IDS.getAll().size)
        Receiver.IDS.getAll().forEach { operationId ->
            assertTrue(receiver.items.any { it.callbackId == operationId })
        }
    }

    @Test
    fun `test no handler for operation results in fail response`() {
        val modifications =
            BaseTreeModificationsSet(MockEntryFactory(IT.injector.getInstance(MdibTypeValidator::class.java)))

        val localMdibAccess = IT.injector.getInstance(LocalMdibAccessFactory::class.java).createLocalMdibAccess()
        localMdibAccess.writeDescription(modifications.createBaseTree())

        scoController = IT.injector.getInstance(ScoControllerFactory::class.java)
            .createScoController(
                eventSourceAccessMock,
                localMdibAccess,
                ReceiverWithMissingHandlers as OperationInvocationReceiver
            )
        val operation = Handles.OPERATION_0
        val instanceIdentifier = InstanceIdentifier().apply {
            rootName = "http://example.com/toor"
            extensionName = "foo"
        }

        val transactionId = 0L
        val callerId = "cn=Bastian Beispielmensch, o=Stiftung Beispielhaft, c=DE;"
        run {
            val expectedPayload = SetValue().apply {
                requestedNumericValue = BigDecimal.TEN
            }
            val response = scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                callerId
            )

            assertEquals(InvocationError.OTH, response.invocationError)
            assertEquals(InvocationState.FAIL, response.invocationState)
            assertEquals(
                OperationInvocationReceiver.MISSING_HANDLER.format(operation),
                response.invocationErrorMessage.first().value
            )
            assertEquals(transactionId, response.transactionId)
        }
        run {
            val response = scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                AbstractSet(),
                callerId
            )

            assertEquals(InvocationError.OTH, response.invocationError)
            assertEquals(InvocationState.FAIL, response.invocationState)
            assertEquals(
                OperationInvocationReceiver.MISSING_HANDLER.format(operation),
                response.invocationErrorMessage.first().value
            )
            assertEquals(transactionId+1, response.transactionId)
        }
    }

    @Test
    fun `test unknown operation throws runtime exception`() {
        val instanceIdentifier = InstanceIdentifier()
        val operation = "<any handle>"
        val expectedPayload = 100

        val exception = assertThrows<RuntimeException> {
            scoController.processIncomingSetOperation(
                operation,
                instanceIdentifier,
                expectedPayload,
                null
            )
        }
        assertTrue(exception.message?.contains(operation) ?: false)
    }

    @Test
    fun `test that an invocation result generated by the SCO controller also triggers an invocation invoked report`() {
        val expectedInstanceIdentifier = InstanceIdentifier().apply {
            rootName = "http://example.com/toor"
            extensionName = "foo"
        }

        val expectedInvocationState = InvocationState.FAIL
        val operationHandle = Handles.OPERATION_0

        val response = scoController.processIncomingSetOperation(
            operationHandle,
            expectedInstanceIdentifier,
            BigDecimal.ZERO,
            null
        )

        verify(eventSourceAccessMock).sendNotification(actionCaptor.capture(), reportCaptor.capture())

        val report = reportCaptor.value.reportPart[0]
        assertEquals(ActionConstants.ACTION_OPERATION_INVOKED_REPORT, actionCaptor.value)
        assertEquals(expectedInvocationState, report.invocationInfo.invocationState)
        assertEquals(operationHandle, report.operationHandleRef)
        assertEquals(response.transactionId, report.invocationInfo.transactionId)
        assertEquals(expectedInstanceIdentifier.rootName, report.invocationSource.rootName)
        assertEquals(expectedInstanceIdentifier.extensionName, report.invocationSource.extensionName)
    }

    data object Receiver : OperationInvocationReceiver {

        val items: MutableList<Item> = mutableListOf()

        object IDS {
            const val ID_SET_VALUE = "setValueOperation"
            const val ID_ACTIVATE_VALUE = "activateForSpecificHandle"
            const val ID_SET_STRING = "setStringOperationSpecific"
            const val ID_SET_ALERT_STATE = "setAlertStateOperation"
            const val ID_SET_METRIC_STATE = "setMetricStateOperation"
            const val ID_SET_CONTEXT_STATE = "setContextStateOperation"
            const val ID_SET_COMPONENT_STATE = "setComponentStateOperation"

            fun getAll() = listOf(
                ID_SET_VALUE,
                ID_ACTIVATE_VALUE,
                ID_SET_STRING,
                ID_SET_ALERT_STATE,
                ID_SET_METRIC_STATE,
                ID_SET_CONTEXT_STATE,
                ID_SET_COMPONENT_STATE
            )
        }

        override fun handleSetValue(
            context: Context,
            operationHandle: String,
            setValue: SetValue
        ): InvocationResponse {
            items.add(Item(ID_SET_VALUE, context, setValue))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }

        override fun handleSetString(
            context: Context,
            operationHandle: String,
            setString: SetString
        ): InvocationResponse {
            items.add(Item(ID_SET_STRING, context, setString))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }

        override fun handleActivate(
            context: Context,
            operationHandle: String,
            activate: Activate
        ): InvocationResponse {
            items.add(Item(ID_ACTIVATE_VALUE, context, activate))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }

        override fun handleSetAlertState(
            context: Context,
            operationHandle: String,
            setAlertState: SetAlertState
        ): InvocationResponse {
            items.add(Item(ID_SET_ALERT_STATE, context, setAlertState))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }

        override fun handleSetMetricState(
            context: Context,
            operationHandle: String,
            setMetricState: SetMetricState
        ): InvocationResponse {
            items.add(Item(ID_SET_METRIC_STATE, context, setMetricState))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }

        override fun handleSetContextState(
            context: Context,
            operationHandle: String,
            setContextState: SetContextState
        ): InvocationResponse {
            items.add(Item(ID_SET_CONTEXT_STATE, context, setContextState))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }

        override fun handleSetComponentState(
            context: Context,
            operationHandle: String,
            setComponentState: SetComponentState
        ): InvocationResponse {
            items.add(Item(ID_SET_COMPONENT_STATE, context, setComponentState))
            return context.createSuccessfulResponse(MdibVersion.create(), InvocationState.FIN)
        }
    }

    data object ReceiverWithMissingHandlers : OperationInvocationReceiver

    data class Item(val callbackId: String, val context: Context, val data: Any)
}