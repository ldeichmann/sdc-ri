@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package com.example.consumer2_trackall_extension;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
