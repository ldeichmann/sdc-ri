package org.somda.sdc.biceps.common

import org.somda.sdc.biceps.model.participant.AbstractContextState
import org.somda.sdc.biceps.model.participant.AbstractDescriptor
import org.somda.sdc.biceps.model.participant.AbstractMultiState
import org.somda.sdc.biceps.model.participant.AbstractState
import org.somda.sdc.biceps.model.participant.ActivateOperationDescriptor
import org.somda.sdc.biceps.model.participant.ActivateOperationState
import org.somda.sdc.biceps.model.participant.AlertConditionDescriptor
import org.somda.sdc.biceps.model.participant.AlertConditionState
import org.somda.sdc.biceps.model.participant.AlertSignalDescriptor
import org.somda.sdc.biceps.model.participant.AlertSignalState
import org.somda.sdc.biceps.model.participant.AlertSystemDescriptor
import org.somda.sdc.biceps.model.participant.AlertSystemState
import org.somda.sdc.biceps.model.participant.BatteryDescriptor
import org.somda.sdc.biceps.model.participant.BatteryState
import org.somda.sdc.biceps.model.participant.ChannelDescriptor
import org.somda.sdc.biceps.model.participant.ChannelState
import org.somda.sdc.biceps.model.participant.ClockDescriptor
import org.somda.sdc.biceps.model.participant.ClockState
import org.somda.sdc.biceps.model.participant.DistributionSampleArrayMetricDescriptor
import org.somda.sdc.biceps.model.participant.DistributionSampleArrayMetricState
import org.somda.sdc.biceps.model.participant.EnsembleContextDescriptor
import org.somda.sdc.biceps.model.participant.EnsembleContextState
import org.somda.sdc.biceps.model.participant.EnumStringMetricDescriptor
import org.somda.sdc.biceps.model.participant.EnumStringMetricState
import org.somda.sdc.biceps.model.participant.LimitAlertConditionDescriptor
import org.somda.sdc.biceps.model.participant.LimitAlertConditionState
import org.somda.sdc.biceps.model.participant.LocationContextDescriptor
import org.somda.sdc.biceps.model.participant.LocationContextState
import org.somda.sdc.biceps.model.participant.MdsDescriptor
import org.somda.sdc.biceps.model.participant.MdsState
import org.somda.sdc.biceps.model.participant.MeansContextDescriptor
import org.somda.sdc.biceps.model.participant.MeansContextState
import org.somda.sdc.biceps.model.participant.NumericMetricDescriptor
import org.somda.sdc.biceps.model.participant.NumericMetricState
import org.somda.sdc.biceps.model.participant.OperatorContextDescriptor
import org.somda.sdc.biceps.model.participant.OperatorContextState
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor
import org.somda.sdc.biceps.model.participant.PatientContextState
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricDescriptor
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricState
import org.somda.sdc.biceps.model.participant.ScoDescriptor
import org.somda.sdc.biceps.model.participant.ScoState
import org.somda.sdc.biceps.model.participant.SetAlertStateOperationDescriptor
import org.somda.sdc.biceps.model.participant.SetAlertStateOperationState
import org.somda.sdc.biceps.model.participant.SetComponentStateOperationDescriptor
import org.somda.sdc.biceps.model.participant.SetComponentStateOperationState
import org.somda.sdc.biceps.model.participant.SetContextStateOperationDescriptor
import org.somda.sdc.biceps.model.participant.SetContextStateOperationState
import org.somda.sdc.biceps.model.participant.SetMetricStateOperationDescriptor
import org.somda.sdc.biceps.model.participant.SetMetricStateOperationState
import org.somda.sdc.biceps.model.participant.SetStringOperationDescriptor
import org.somda.sdc.biceps.model.participant.SetStringOperationState
import org.somda.sdc.biceps.model.participant.SetValueOperationDescriptor
import org.somda.sdc.biceps.model.participant.SetValueOperationState
import org.somda.sdc.biceps.model.participant.StringMetricDescriptor
import org.somda.sdc.biceps.model.participant.StringMetricState
import org.somda.sdc.biceps.model.participant.SystemContextDescriptor
import org.somda.sdc.biceps.model.participant.SystemContextState
import org.somda.sdc.biceps.model.participant.VmdDescriptor
import org.somda.sdc.biceps.model.participant.VmdState
import org.somda.sdc.biceps.model.participant.WorkflowContextDescriptor
import org.somda.sdc.biceps.model.participant.WorkflowContextState

/**
 * Represents a pair of objects.
 *
 * This class is a sealed class that contains two subclasses: SingleStatePair and MultiStatePair.
 * Each subclass represents a specific type of pair.
 *
 * @see SingleStatePair
 * @see MultiStatePair
 */
sealed class Pair {
    data class SingleStatePair(val pair: org.somda.sdc.biceps.common.SingleStatePair): Pair()
    data class MultiStatePair(val pair: org.somda.sdc.biceps.common.MultiStatePair): Pair()

    val handle: String
        get() = when (this) {
            is SingleStatePair -> this.pair.handle
            is MultiStatePair -> this.pair.handle
        }

    val descriptor: AbstractDescriptor
        get() = when (this) {
            is SingleStatePair -> this.pair.abstractDescriptor
            is MultiStatePair -> this.pair.abstractDescriptor
        }

    val states: List<AbstractState>
        get() = when (this) {
            is SingleStatePair -> listOf(this.pair.abstractState)
            is MultiStatePair -> this.pair.abstractContextStates
        }

    companion object {
        /**
         * Tries to create a Pair object based on the provided AbstractDescriptor and AbstractState.
         *
         * @param descriptor the AbstractDescriptor object used to create the Pair.
         * @param state the AbstractState object used to create the Pair.
         * @return the created Pair object if successful, otherwise null.
         */
        @JvmStatic
        fun tryFrom(descriptor: AbstractDescriptor, state: AbstractState): Pair? {
            return when (state) {
                is AbstractMultiState -> org.somda.sdc.biceps.common.MultiStatePair.tryFrom(descriptor, listOf(state))?.let { MultiStatePair(it) }
                else -> org.somda.sdc.biceps.common.SingleStatePair.tryFrom(descriptor, state)?.let { SingleStatePair(it) }
            }
        }

        /**
         * Tries to create a Pair object based on the provided AbstractDescriptor and AbstractState.
         *
         * @param descriptor the AbstractDescriptor object used to create the Pair.
         * @param state the AbstractState object used to create the Pair.
         * @return the created Pair object if successful, otherwise null.
         * @throws PairException if pair contains mismatching types.
         */
        @JvmStatic
        @Throws(PairException::class)
        fun tryFromThrowing(descriptor: AbstractDescriptor, state: AbstractState): Pair {
            val pair = when (state) {
                is AbstractMultiState -> org.somda.sdc.biceps.common.MultiStatePair.tryFrom(descriptor, listOf(state))?.let { MultiStatePair(it) }
                else -> org.somda.sdc.biceps.common.SingleStatePair.tryFrom(descriptor, state)?.let { SingleStatePair(it) }
            }
            return pair ?: throw PairException("Could not create pair from input, descriptor and state types do not match")
        }

        /**
         * Tries to create a new instance of [Pair] by converting the given [descriptor] and [state]
         * using the [org.somda.sdc.biceps.common.MultiStatePair.tryFrom] method.
         * If the conversion is successful, returns the new [Pair] instance wrapped in a [MultiStatePair].
         * Otherwise, returns null.
         *
         * @param descriptor The [AbstractDescriptor] to convert.
         * @param state The list of [AbstractMultiState] objects to convert.
         * @return A new [Pair] instance wrapped in a [MultiStatePair] if conversion is successful, otherwise null.
         */
        @JvmStatic
        fun <T: AbstractMultiState> tryFrom(descriptor: AbstractDescriptor, state: List<T>): Pair? {
            return org.somda.sdc.biceps.common.MultiStatePair.tryFrom(descriptor, state)?.let { MultiStatePair(it) }
        }

        /**
         * Tries to create a Pair object based on the provided AbstractDescriptor and AbstractState.
         *
         * @param descriptor the AbstractDescriptor object used to create the Pair.
         * @param state the AbstractState object used to create the Pair.
         * @return the created Pair object if successful, otherwise null.
         * @throws PairException if pair contains mismatching types.
         */
        @JvmStatic
        @Throws(PairException::class)
        fun <T: AbstractMultiState> tryFromThrowing(descriptor: AbstractDescriptor, state: List<T>): Pair {
            val pair = org.somda.sdc.biceps.common.MultiStatePair.tryFrom(descriptor, state)?.let { MultiStatePair(it) }
            return pair ?: throw PairException("Could not create pair from input, descriptor and state types do not match")
        }
    }
}

class PairException(message: String? = null, cause: Throwable? = null): Exception(message, cause) {

}

sealed class SingleStatePair(val abstractDescriptor: AbstractDescriptor, val abstractState: AbstractState) {
    data class EnumStringMetric(val descriptor: EnumStringMetricDescriptor, val state: EnumStringMetricState): SingleStatePair(descriptor, state)
    data class StringMetric(val descriptor: StringMetricDescriptor, val state: StringMetricState): SingleStatePair(descriptor, state)
    data class NumericMetric(val descriptor: NumericMetricDescriptor, val state: NumericMetricState): SingleStatePair(descriptor, state)
    data class RealTimeSampleArrayMetric(val descriptor: RealTimeSampleArrayMetricDescriptor, val state: RealTimeSampleArrayMetricState): SingleStatePair(descriptor, state)
    data class DistributionSampleArrayMetric(val descriptor: DistributionSampleArrayMetricDescriptor, val state: DistributionSampleArrayMetricState): SingleStatePair(descriptor, state)
    data class LimitAlertCondition(val descriptor: LimitAlertConditionDescriptor, val state: LimitAlertConditionState): SingleStatePair(descriptor, state)
    data class AlertCondition(val descriptor: AlertConditionDescriptor, val state: AlertConditionState): SingleStatePair(descriptor, state)
    data class AlertSignal(val descriptor: AlertSignalDescriptor, val state: AlertSignalState): SingleStatePair(descriptor, state)
    data class AlertSystem(val descriptor: AlertSystemDescriptor, val state: AlertSystemState): SingleStatePair(descriptor, state)
    data class Mds(val descriptor: MdsDescriptor, val state: MdsState): SingleStatePair(descriptor, state)
    data class Vmd(val descriptor: VmdDescriptor, val state: VmdState): SingleStatePair(descriptor, state)
    data class Channel(val descriptor: ChannelDescriptor, val state: ChannelState): SingleStatePair(descriptor, state)
    data class Sco(val descriptor: ScoDescriptor, val state: ScoState): SingleStatePair(descriptor, state)
    data class SystemContext(val descriptor: SystemContextDescriptor, val state: SystemContextState): SingleStatePair(descriptor, state)
    data class Battery(val descriptor: BatteryDescriptor, val state: BatteryState): SingleStatePair(descriptor, state)
    data class Clock(val descriptor: ClockDescriptor, val state: ClockState): SingleStatePair(descriptor, state)
    data class SetValueOperation(val descriptor: SetValueOperationDescriptor, val state: SetValueOperationState): SingleStatePair(descriptor, state)
    data class SetStringOperation(val descriptor: SetStringOperationDescriptor, val state: SetStringOperationState): SingleStatePair(descriptor, state)
    data class SetAlertStateOperation(val descriptor: SetAlertStateOperationDescriptor, val state: SetAlertStateOperationState): SingleStatePair(descriptor, state)
    data class SetMetricStateOperation(val descriptor: SetMetricStateOperationDescriptor, val state: SetMetricStateOperationState): SingleStatePair(descriptor, state)
    data class SetComponentStateOperation(val descriptor: SetComponentStateOperationDescriptor, val state: SetComponentStateOperationState): SingleStatePair(descriptor, state)
    data class SetContextStateOperation(val descriptor: SetContextStateOperationDescriptor, val state: SetContextStateOperationState): SingleStatePair(descriptor, state)
    data class ActivateOperation(val descriptor: ActivateOperationDescriptor, val state: ActivateOperationState): SingleStatePair(descriptor, state)

    val handle: String
        get() = this.abstractDescriptor.handle
    companion object {
        fun tryFrom(descriptor: AbstractDescriptor, state: AbstractState): SingleStatePair? {
            return when {
                // order matters
                descriptor is EnumStringMetricDescriptor && state is EnumStringMetricState -> EnumStringMetric(descriptor, state)
                descriptor is StringMetricDescriptor && state is StringMetricState -> StringMetric(descriptor, state)

                descriptor is NumericMetricDescriptor && state is NumericMetricState -> NumericMetric(descriptor, state)
                descriptor is RealTimeSampleArrayMetricDescriptor && state is RealTimeSampleArrayMetricState -> RealTimeSampleArrayMetric(descriptor, state)
                descriptor is DistributionSampleArrayMetricDescriptor && state is DistributionSampleArrayMetricState -> DistributionSampleArrayMetric(descriptor, state)

                // order matters
                descriptor is LimitAlertConditionDescriptor && state is LimitAlertConditionState -> LimitAlertCondition(descriptor, state)
                descriptor is AlertConditionDescriptor && state is AlertConditionState -> AlertCondition(descriptor, state)

                descriptor is AlertSignalDescriptor && state is AlertSignalState -> AlertSignal(descriptor, state)
                descriptor is AlertSystemDescriptor && state is AlertSystemState -> AlertSystem(descriptor, state)
                descriptor is MdsDescriptor && state is MdsState -> Mds(descriptor, state)
                descriptor is VmdDescriptor && state is VmdState -> Vmd(descriptor, state)
                descriptor is ChannelDescriptor && state is ChannelState -> Channel(descriptor, state)
                descriptor is ScoDescriptor && state is ScoState -> Sco(descriptor, state)
                descriptor is SystemContextDescriptor && state is SystemContextState -> SystemContext(descriptor, state)
                descriptor is BatteryDescriptor && state is BatteryState -> Battery(descriptor, state)
                descriptor is ClockDescriptor && state is ClockState -> Clock(descriptor, state)
                descriptor is SetValueOperationDescriptor && state is SetValueOperationState -> SetValueOperation(descriptor, state)
                descriptor is SetStringOperationDescriptor && state is SetStringOperationState -> SetStringOperation(descriptor, state)
                descriptor is SetAlertStateOperationDescriptor && state is SetAlertStateOperationState -> SetAlertStateOperation(descriptor, state)
                descriptor is SetMetricStateOperationDescriptor && state is SetMetricStateOperationState -> SetMetricStateOperation(descriptor, state)
                descriptor is SetComponentStateOperationDescriptor && state is SetComponentStateOperationState -> SetComponentStateOperation(descriptor, state)
                descriptor is SetContextStateOperationDescriptor && state is SetContextStateOperationState -> SetContextStateOperation(descriptor, state)
                descriptor is ActivateOperationDescriptor && state is ActivateOperationState -> ActivateOperation(descriptor, state)
                else -> null
            }
        }
    }
}

sealed class MultiStatePair(val abstractDescriptor: AbstractDescriptor, val abstractContextStates: List<AbstractContextState>) {
    data class PatientContext(val descriptor: PatientContextDescriptor, val state: List<PatientContextState>): MultiStatePair(descriptor, state)
    data class LocationContext(val descriptor: LocationContextDescriptor, val state: List<LocationContextState>): MultiStatePair(descriptor, state)
    data class EnsembleContext(val descriptor: EnsembleContextDescriptor, val state: List<EnsembleContextState>): MultiStatePair(descriptor, state)
    data class WorkflowContext(val descriptor: WorkflowContextDescriptor, val state: List<WorkflowContextState>): MultiStatePair(descriptor, state)
    data class MeansContext(val descriptor: MeansContextDescriptor, val state: List<MeansContextState>): MultiStatePair(descriptor, state)
    data class OperatorContext(val descriptor: OperatorContextDescriptor, val state: List<OperatorContextState>): MultiStatePair(descriptor, state)

    val handle: String
        get() = this.abstractDescriptor.handle

    companion object {
        fun <T: AbstractMultiState> tryFrom(descriptor: AbstractDescriptor, state: List<T>): MultiStatePair? {
            @Suppress("UNCHECKED_CAST") // checked by state.all
            return when {
                descriptor is PatientContextDescriptor && state.all { it is PatientContextState } -> PatientContext(descriptor, state as List<PatientContextState>)
                descriptor is LocationContextDescriptor && state.all { it is LocationContextState } -> LocationContext(descriptor, state as List<LocationContextState>)
                descriptor is EnsembleContextDescriptor && state.all { it is EnsembleContextState } -> EnsembleContext(descriptor, state as List<EnsembleContextState>)
                descriptor is WorkflowContextDescriptor && state.all { it is WorkflowContextState } -> WorkflowContext(descriptor, state as List<WorkflowContextState>)
                descriptor is MeansContextDescriptor && state.all { it is MeansContextState } -> MeansContext(descriptor, state as List<MeansContextState>)
                descriptor is OperatorContextDescriptor && state.all { it is OperatorContextState } -> OperatorContext(descriptor, state as List<OperatorContextState>)
                else -> null
            }
        }
    }
}