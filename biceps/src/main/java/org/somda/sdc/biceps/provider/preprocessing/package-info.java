/**
 * Preprocessing actions for {@linkplain org.somda.sdc.biceps.provider.access.LocalMdibAccess}.
 */
@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.biceps.provider.preprocessing;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
