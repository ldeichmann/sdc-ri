package org.somda.sdc.biceps.common

import org.jvnet.jaxb.lang.CopyStrategy
import org.jvnet.jaxb.lang.CopyTo
import org.jvnet.jaxb.locator.ObjectLocator
import org.somda.sdc.biceps.model.participant.AbstractAlertState
import org.somda.sdc.biceps.model.participant.AbstractContextState
import org.somda.sdc.biceps.model.participant.AbstractDeviceComponentState
import org.somda.sdc.biceps.model.participant.AbstractMetricState
import org.somda.sdc.biceps.model.participant.AbstractOperationState
import org.somda.sdc.biceps.model.participant.AbstractState
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricState
import java.util.*

sealed class MdibStateModifications: CopyTo {
    data class Alert(val alertStates: List<AbstractAlertState>): MdibStateModifications()
    data class Component(val componentStates: List<AbstractDeviceComponentState>): MdibStateModifications()
    data class Context(val contextStates: List<AbstractContextState>): MdibStateModifications()
    data class Metric(val metricStates: List<AbstractMetricState>): MdibStateModifications()
    data class Operation(val operationStates: List<AbstractOperationState>): MdibStateModifications()
    data class Waveform(val waveformStates: List<RealTimeSampleArrayMetricState>): MdibStateModifications()

    /**
     * Retrieve an unmodifiable [List] of [AbstractState] in this modification.
     */
    val states: List<AbstractState>
        get() = Collections.unmodifiableList(when (this) {
            is Alert -> this.alertStates
            is Component -> this.componentStates
            is Context -> this.contextStates
            is Metric -> this.metricStates
            is Operation -> this.operationStates
            is Waveform -> this.waveformStates
        })

    val size: Int
        get() = states.size

    val isEmpty: Boolean
        get() = size == 0

    override fun copyTo(p0: Any?): Any {
        return when (this) {
            is Alert -> this.copy()
            is Component -> this.copy()
            is Context -> this.copy()
            is Metric -> this.copy()
            is Operation -> this.copy()
            is Waveform -> this.copy()
        }
    }

    override fun copyTo(p0: ObjectLocator?, p1: Any?, p2: CopyStrategy?): Any {
        return this.copyTo(p1)
    }

    override fun createNewInstance(): Any {
        return when (this) {
            is Alert -> Alert(emptyList())
            is Component -> Component(emptyList())
            is Context -> Context(emptyList())
            is Metric -> Metric(emptyList())
            is Operation -> Operation(emptyList())
            is Waveform -> Waveform(emptyList())
        }
    }
}