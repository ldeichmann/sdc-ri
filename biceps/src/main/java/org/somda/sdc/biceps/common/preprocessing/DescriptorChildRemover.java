package org.somda.sdc.biceps.common.preprocessing;

import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.SingleStatePair;
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment;
import org.somda.sdc.biceps.common.storage.MdibStorageRead;
import org.somda.sdc.biceps.model.participant.AlertSystemDescriptor;
import org.somda.sdc.biceps.model.participant.ChannelDescriptor;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.ScoDescriptor;
import org.somda.sdc.biceps.model.participant.SystemContextDescriptor;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Removes children from descriptors in order to avoid redundant information in the MDIB storage.
 */
public class DescriptorChildRemover implements DescriptionPreprocessingSegment {

    @Override
    public List<MdibDescriptionModification> process(
        List<MdibDescriptionModification> allModifications,
        MdibStorageRead storage
    ) throws Exception {
        return allModifications.stream()
            .map(it -> {
                if (it instanceof MdibDescriptionModification.Insert) {
                    MdibDescriptionModification.Insert insert = (MdibDescriptionModification.Insert) it;
                    if (insert.getPair() instanceof Pair.SingleStatePair) {
                        // process
                        Pair.SingleStatePair singleStatePair = (Pair.SingleStatePair) insert.getPair();
                        return new MdibDescriptionModification.Insert(
                                new Pair.SingleStatePair(processSingleState(singleStatePair.getPair())),
                                insert.getParentHandle()
                        );
                    }
                    // keep same
                    return insert;
                } else if (it instanceof MdibDescriptionModification.Update) {
                    MdibDescriptionModification.Update update = (MdibDescriptionModification.Update) it;
                    if (update.getPair() instanceof Pair.SingleStatePair) {
                        // process
                        Pair.SingleStatePair singleStatePair = (Pair.SingleStatePair) update.getPair();
                        return new MdibDescriptionModification.Update(
                                new Pair.SingleStatePair(processSingleState(singleStatePair.getPair()))
                        );
                    }
                    // keep same
                    return update;
                } else {
                    return it;
                }
            })
            .collect(Collectors.toList());
    }

    private SingleStatePair processSingleState(SingleStatePair singleStatePair) {
        if (singleStatePair instanceof SingleStatePair.Mds) {
            return new SingleStatePair.Mds(
                removeChildren(((SingleStatePair.Mds) singleStatePair).getDescriptor()),
                ((SingleStatePair.Mds) singleStatePair).getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.Vmd) {
            return new SingleStatePair.Vmd(
                removeChildren(((SingleStatePair.Vmd) singleStatePair).getDescriptor()),
                ((SingleStatePair.Vmd) singleStatePair).getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.Channel) {
            return new SingleStatePair.Channel(
                removeChildren(((SingleStatePair.Channel) singleStatePair).getDescriptor()),
                ((SingleStatePair.Channel) singleStatePair).getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.Sco) {
            return new SingleStatePair.Sco(
                removeChildren(((SingleStatePair.Sco) singleStatePair).getDescriptor()),
                ((SingleStatePair.Sco) singleStatePair).getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.SystemContext) {
            return new SingleStatePair.SystemContext(
                removeChildren(((SingleStatePair.SystemContext) singleStatePair).getDescriptor()),
                ((SingleStatePair.SystemContext) singleStatePair).getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.AlertSystem) {
            return new SingleStatePair.AlertSystem(
                removeChildren(((SingleStatePair.AlertSystem) singleStatePair).getDescriptor()),
                ((SingleStatePair.AlertSystem) singleStatePair).getState()
            );
        } else {
            return singleStatePair;
        }
    }

    private AlertSystemDescriptor removeChildren(AlertSystemDescriptor descriptor) {
        descriptor.setAlertSignal(null);
        descriptor.setAlertCondition(null);
        return descriptor;
    }

    private SystemContextDescriptor removeChildren(SystemContextDescriptor descriptor) {
        descriptor.setLocationContext(null);
        descriptor.setPatientContext(null);
        descriptor.setMeansContext(null);
        descriptor.setOperatorContext(null);
        descriptor.setWorkflowContext(null);
        descriptor.setEnsembleContext(null);
        return descriptor;
    }

    private ScoDescriptor removeChildren(ScoDescriptor descriptor) {
        descriptor.setOperation(null);
        return descriptor;
    }

    private ChannelDescriptor removeChildren(ChannelDescriptor descriptor) {
        descriptor.setMetric(null);
        return descriptor;
    }

    private VmdDescriptor removeChildren(VmdDescriptor descriptor) {
        descriptor.setChannel(null);
        descriptor.setAlertSystem(null);
        return descriptor;
    }

    /**
     * Removes the children from a provided mds.
     * <p>
     * Removes the battery, clock, system context, vmd, alert system and sco
     * @param mds without the children
     */
    private MdsDescriptor removeChildren(MdsDescriptor mds) {
        mds.setBattery(null);
        mds.setClock(null);
        mds.setSystemContext(null);
        mds.setVmd(null);
        mds.setAlertSystem(null);
        mds.setSco(null);
        return mds;
    }
}
