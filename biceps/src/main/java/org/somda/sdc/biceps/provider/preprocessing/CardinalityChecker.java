package org.somda.sdc.biceps.provider.preprocessing;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibTreeValidator;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment;
import org.somda.sdc.biceps.common.storage.MdibStorageRead;
import org.somda.sdc.common.CommonConfig;
import org.somda.sdc.common.logging.InstanceLogger;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Preprocessing segment that verifies correctness of child cardinality.
 * <p>
 * In BICEPS descriptors of a certain type can appear one or many times as a child of another descriptor.
 * This checker guarantees that no child is inserted twice if the maximum allowed number of children is one.
 */
public class CardinalityChecker implements DescriptionPreprocessingSegment {
    private static final Logger LOG = LogManager.getLogger(CardinalityChecker.class);

    private final MdibTreeValidator treeValidator;
    private final Logger instanceLogger;

    @Inject
    CardinalityChecker(MdibTreeValidator treeValidator,
                       @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier) {
        this.instanceLogger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier);
        this.treeValidator = treeValidator;
    }

    private boolean pairVariantEqual(Pair a, Pair b) {
        if (a instanceof Pair.SingleStatePair && b instanceof Pair.SingleStatePair) {
            Pair.SingleStatePair aTyped = (Pair.SingleStatePair) a;
            Pair.SingleStatePair bTyped = (Pair.SingleStatePair) b;
            return aTyped.getPair().getClass() == bTyped.getPair().getClass();
        } else if (a instanceof Pair.MultiStatePair && b instanceof  Pair.MultiStatePair) {
            Pair.MultiStatePair aTyped = (Pair.MultiStatePair) a;
            Pair.MultiStatePair bTyped = (Pair.MultiStatePair) b;
            return aTyped.getPair().getClass() == bTyped.getPair().getClass();
        } else {
            return false;
        }
    }

    private boolean isSameTypeInModification(
        List<MdibDescriptionModification> modifications,
        Pair pair,
        String parent
    ) {
        return modifications.stream()
                // get pairs from modifications
                .filter(it -> it instanceof MdibDescriptionModification.Insert)
                .map(it -> (MdibDescriptionModification.Insert) it)
                // remove modification that contains the same handle to not check against itself
                .filter(it -> !pair.getHandle().equals(it.getDescriptorHandle()))
                // check for parent equality
                .filter(it -> parent.equals(it.getParentHandle()))
                // if parents are equal, check for type equality
                .anyMatch(it -> pairVariantEqual(it.getPair(), pair));
    }

    @Override
    public List<MdibDescriptionModification> process(
            List<MdibDescriptionModification> allModifications,
            MdibStorageRead storage
    ) throws CardinalityException {

        var errors = allModifications.stream()
                .filter(it -> it instanceof MdibDescriptionModification.Insert)
                .map(it -> (MdibDescriptionModification.Insert) it)
                // filter mds
                .filter(it -> it.getParentHandle() != null)
                .filter(it -> {
                    instanceLogger.debug(
                        "Validating %s %s relationship",
                        it.getParentHandle(), it.getDescriptorHandle()
                    );

                    if (
                        isSameTypeInModification(allModifications, it.getPair(), it.getParentHandle())
                        && !treeValidator.isManyAllowed(it.getPair().getDescriptor())
                    ) {
                        // There is no other child of the same type to be inserted below the parent
                        return true;
                    }

                    final var parentEntity = storage.getEntity(it.getParentHandle());
                    return parentEntity
                        .filter(mdibEntity -> !(
                            storage.getChildrenByType(
                                it.getParentHandle(),
                                it.getPair().getDescriptor().getClass()).isEmpty()
                                || treeValidator.isManyAllowed(it.getPair().getDescriptor())
                            )
                        ).isPresent();

                })
                .collect(Collectors.toList());

        if (!errors.isEmpty()) {
            var errorStrings = errors.stream()
                    .map(it -> String.format("%s and parent %s", it.getDescriptorHandle(), it.getParentHandle()))
                    .collect(Collectors.joining(", "));
            throw new CardinalityException(
                String.format("A cardinality error occurred, conflicts were %s", errorStrings)
            );
        }
        return allModifications;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
