package org.somda.sdc.biceps.consumer.preprocessing;

import com.google.inject.Inject;
import org.somda.sdc.biceps.common.MdibStateModifications;
import org.somda.sdc.biceps.common.storage.MdibStorageRead;
import org.somda.sdc.biceps.common.storage.StatePreprocessingSegment;
import org.somda.sdc.biceps.model.participant.AbstractMultiState;

import java.util.HashMap;
import java.util.Map;

/**
 * Preprocessing segment that throws an Exception, if two context states have the same handle, but different
 * descriptor handles. Which could mean that the context state handle is used multiple times or that the context
 * state was assigned to a new descriptor. Both operations are not allowed.
 */
public class DuplicateContextStateHandleHandler implements StatePreprocessingSegment {
    private Map<String, AbstractMultiState> allContextStates;

    @Inject
    DuplicateContextStateHandleHandler() {}

    @Override
    public MdibStateModifications beforeFirstModification(
        MdibStateModifications modifications,
        MdibStorageRead mdibStorage
    ) {
        // this check is only necessary for context changes
        if (!(modifications instanceof MdibStateModifications.Context)) {
            return modifications;
        }

        allContextStates = new HashMap<>();
        for (var state : mdibStorage.getStatesByType(AbstractMultiState.class)) {
            allContextStates.put(state.getHandle(), state);
        }

        return modifications;
    }

    @Override
    public MdibStateModifications process(MdibStateModifications modifications, MdibStorageRead storage)
            throws DuplicateContextStateHandleException {
        // this check is only necessary for context changes
        if (!(modifications instanceof MdibStateModifications.Context)) {
            return modifications;
        }
        MdibStateModifications.Context contextModifications = (MdibStateModifications.Context) modifications;

        for (var multiState: contextModifications.getContextStates()) {
            String handle = multiState.getHandle();

            if (allContextStates.containsKey(handle)) {
                var existingContextState = allContextStates.get(handle);
                if (!existingContextState.getDescriptorHandle().equals(multiState.getDescriptorHandle())) {
                    throw new DuplicateContextStateHandleException(String.format("Two different descriptors:"
                                    + " %s and %s can not have the same context state: %s",
                            existingContextState.getDescriptorHandle(),
                            multiState.getDescriptorHandle(),
                            multiState.getHandle()));
                }
            }
            allContextStates.put(handle, multiState);
        }
        return modifications;
    }

    @Override
    public MdibStateModifications afterLastModification(
        MdibStateModifications modifications,
        MdibStorageRead mdibStorage
    ) {
        allContextStates = null;
        return modifications;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
