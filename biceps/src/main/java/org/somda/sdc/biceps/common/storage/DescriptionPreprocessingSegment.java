package org.somda.sdc.biceps.common.storage;

import org.somda.sdc.biceps.common.MdibDescriptionModification;

import java.util.List;

/**
 * A segment that is applied during description modifications.
 */
public interface DescriptionPreprocessingSegment {
    /**
     * Function that is invoked before the first modification in the processing chain is applied.
     * <p>
     * Default behavior is <em>do nothing</em>.
     *
     * @param modifications all modifications for preprocessing.
     * @param mdibStorage   the MDIB storage to be used by the callback.
     * @return list of pre-processed modifications
     */
    default List<MdibDescriptionModification> beforeFirstModification(
        List<MdibDescriptionModification> modifications,
        MdibStorageRead mdibStorage
    ) {
        return modifications;
    }

    /**
     * Function that is invoked after the last modification in the processing chain has been applied.
     * <p>
     * Default behavior is <em>do nothing</em>.
     *
     * @param modifications all modifications for preprocessing.
     * @param mdibStorage   the MDIB storage to be used by the callback.
     * @return list of post-processed modifications
     */
    default List<MdibDescriptionModification> afterLastModification(
        List<MdibDescriptionModification> modifications,
        MdibStorageRead mdibStorage
    ) {
        return modifications;
    }

    /**
     * In a sequence of modifications this function processes one modification.
     *
     * @param modifications all modifications for processing.
     * @param mdibStorage   the MDIB storage for access.
     * @throws Exception an arbitrary exception if something goes wrong.
     * @return list of processed modifications
     */
    List<MdibDescriptionModification> process(
        List<MdibDescriptionModification> modifications,
        MdibStorageRead mdibStorage
    ) throws Exception;
}
