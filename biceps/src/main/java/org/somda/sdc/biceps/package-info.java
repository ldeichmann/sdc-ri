/**
 * Domain layer implementation that comprises local and remote MDIB access with consistency checks and versioning.
 */
@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.biceps;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
