package org.somda.sdc.biceps.consumer.access.factory;

import org.somda.sdc.biceps.common.CommonConfig;
import org.somda.sdc.biceps.consumer.access.RemoteMdibAccess;

/**
 * Factory to create {@linkplain RemoteMdibAccess} instances.
 */
public interface RemoteMdibAccessFactory {
    /**
     * Creates a local mdib access with an MDIB version that has a random sequence id.
     * <p>
     * The preprocessing steps are visited in the order configured through
     * {@link CommonConfig#CONSUMER_DESCRIPTION_PREPROCESSING_SEGMENTS} and
     * {@link CommonConfig#CONSUMER_STATE_PREPROCESSING_SEGMENTS}
     *
     * @return the remote mdib access instance.
     */
    RemoteMdibAccess createRemoteMdibAccess();
}
