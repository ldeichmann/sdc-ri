package org.somda.sdc.biceps.provider.preprocessing

import com.google.inject.Inject
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibEntity
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.Pair
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead
import org.somda.sdc.biceps.common.storage.StatePreprocessingSegment
import org.somda.sdc.biceps.model.participant.AbstractContextDescriptor
import org.somda.sdc.biceps.model.participant.AbstractContextState
import org.somda.sdc.biceps.model.participant.AbstractMultiState
import org.somda.sdc.biceps.model.participant.AbstractState
import org.somda.sdc.biceps.model.participant.MdsDescriptor
import org.somda.sdc.common.util.copyTyped
import java.math.BigInteger
import kotlin.jvm.optionals.getOrNull

/**
 * Represents a pair of versions for a descriptor and state.
 *
 * @property descriptorVersion the version for the descriptor.
 * @property stateVersion the version for the state.
 */
data class VersionPair(
    val descriptorVersion: BigInteger?,
    val stateVersion: BigInteger?
) {

    fun incDesc() = this.copy(descriptorVersion = this.descriptorVersion?.add(BigInteger.ONE) ?: BigInteger.ZERO)

    fun incState() = this.copy(stateVersion = this.stateVersion?.add(BigInteger.ONE) ?: BigInteger.ZERO)

    companion object {
        fun fromEntity(entity: MdibEntity): VersionPair {
            val stateVersion = when (entity.descriptor) {
                is AbstractContextDescriptor -> null
                // must be present
                else -> entity.states[0].stateVersion ?: BigInteger.ZERO
            }

            return VersionPair(
                entity.descriptor.descriptorVersion ?: BigInteger.ZERO,
                stateVersion
            )
        }

        fun fromContext(context: AbstractContextState): VersionPair {
            return VersionPair(
                context.descriptorVersion ?: BigInteger.ZERO,
                context.stateVersion ?: BigInteger.ZERO
            )
        }

        fun default() = VersionPair(null, null)
    }
}

/**
 * Represents a version number.
 *
 * @property number the version number. Can be null.
 * @constructor creates a Version object with the given version number.
 */
data class Version(
    val number: BigInteger?
) {

    fun inc() = Version(this.number?.add(BigInteger.ONE) ?: BigInteger.ZERO)
    companion object {
        fun fromEntity(entity: MdibEntity) =
            Version(entity.descriptor.descriptorVersion ?: BigInteger.ZERO)

        fun fromContextState(contextState: AbstractContextState) =
            Version(contextState.stateVersion ?: BigInteger.ZERO)

        fun default() = Version(null)
    }
}

/**
 * Class for handling versions of artifacts and tracking updates.
 */
open class VersionHandlerInterior {
    private val versionsOfDeletedArtifacts = mutableMapOf<String, VersionPair>()
    private val updatedParents = mutableSetOf<String>()
    private val updatedVersions = mutableMapOf<String, VersionPair>()
    private val deleted = mutableSetOf<String>()

    fun clearUpdatedParents() = this.updatedParents.clear()
    fun clearUpdatedVersions() = this.updatedVersions.clear()
    fun clearDeletedHandles() = this.deleted.clear()

    private fun getVersionOfDeletedArtifacts(handle: String): VersionPair? {
        return versionsOfDeletedArtifacts[handle]
    }

    fun setVersionOfDeletedArtifacts(handle: String, versionPair: VersionPair) {
        versionsOfDeletedArtifacts[handle] = versionPair
    }

    fun getUpdatedVersions(handle: String): VersionPair? {
        return updatedVersions[handle]
    }

    private fun setUpdatedVersions(handle: String, versionPair: VersionPair) {
        updatedVersions[handle] = versionPair
    }

    fun getDeletedHandles(): Set<String> {
        return this.deleted
    }

    fun addDeletedHandles(handles: Collection<String>) {
        deleted.addAll(handles)
    }

    fun isUpdatedParents(handle: String): Boolean {
        return updatedParents.contains(handle)
    }

    private fun addUpdatedParents(handle: String) {
        updatedParents.add(handle)
    }

    fun getVersionDescriptor(storage: MdibStorageRead, handle: String): Version? {
        return storage.getEntity(handle).getOrNull()?.let {
            Version.fromEntity(it)
        } ?: getVersionOfDeletedArtifacts(handle)?.let { Version(it.descriptorVersion) }
    }

    fun getSavedVersionDescriptor(handle: String): Version? {
        return getUpdatedVersions(handle)?.let { Version(it.descriptorVersion) }
    }

    fun getVersionContextState(storage: MdibStorageRead, handle: String): Version? {
        return storage.contextStates
            .filter { it.handle == handle }
            // at most one now
            .map { Version.fromContextState(it) }
            .firstOrNull() ?: getVersionOfDeletedArtifacts(handle)
            ?.let { Version(it.stateVersion) }
    }

    fun getSavedVersionState(handle: String): Version? {
        return getUpdatedVersions(handle)?.let { Version((it.stateVersion)) }
    }

    fun getVersionPair(storage: MdibStorageRead, handle: String): VersionPair? {
        return storage.getEntity(handle).getOrNull()?.let { VersionPair.fromEntity(it) }
            ?: getVersionOfDeletedArtifacts(handle)
    }

    fun getVersionPairContext(storage: MdibStorageRead, handle: String): VersionPair? {
        return storage.contextStates
            .filter { it.handle == handle }
            .map { VersionPair.fromContext(it) }
            .firstOrNull()
            ?: getVersionOfDeletedArtifacts(handle)
    }

    fun getSavedVersionPair(handle: String): VersionPair? =
        getUpdatedVersions(handle)

    fun setUpdated(pair: Pair) {
        addUpdatedParents(pair.handle)

        when (pair) {
            is Pair.SingleStatePair -> {
                setUpdatedVersions(pair.handle, VersionPair(pair.pair.abstractDescriptor.descriptorVersion, pair.pair.abstractState.stateVersion))
            }
            is Pair.MultiStatePair -> {
                setUpdatedVersions(pair.handle, VersionPair(pair.pair.abstractDescriptor.descriptorVersion, null))

                // insert all states, expensive
                pair.pair.abstractContextStates.forEach {
                    setUpdatedVersions(it.handle, VersionPair(it.descriptorVersion, it.stateVersion))
                }
            }
        }
    }
}

/**
 * Preprocessing segment that manages BICEPS versioning.
 */
class VersionHandler @Inject constructor() : DescriptionPreprocessingSegment, StatePreprocessingSegment {

    private val interior = VersionHandlerInterior()

    override fun beforeFirstModification(
        modifications: List<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): List<MdibDescriptionModification> {
        interior.clearUpdatedParents()
        interior.clearUpdatedVersions()
        interior.clearDeletedHandles()

        val deletions = modifications
            .filterIsInstance<MdibDescriptionModification.Delete>()
            .map { it.handle }
            .toList()
        interior.addDeletedHandles(deletions)
        return modifications
    }

    override fun process(
        modifications: List<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): List<MdibDescriptionModification> {
        return modifications
            .map {
                when (it) {
                    is MdibDescriptionModification.Delete -> processDelete(mdibStorage, it.handle)
                    is MdibDescriptionModification.Insert -> processInsert(mdibStorage, it.pair, it.parent)
                    is MdibDescriptionModification.Update -> processUpdate(mdibStorage, it.pair)
                }
            }
            .flatten()
            .toList()
    }

    override fun beforeFirstModification(
        modifications: MdibStateModifications,
        mdibStorage: MdibStorageRead
    ): MdibStateModifications {
        interior.clearUpdatedParents()
        interior.clearUpdatedVersions()
        return modifications
    }

    override fun process(
        modifications: MdibStateModifications,
        storage: MdibStorageRead
    ): MdibStateModifications {
        return when (modifications) {
            is MdibStateModifications.Alert -> {
                val updatedStates = modifications.alertStates
                    .map { processStateUpdateSingleState(storage, it) }
                    .toList()
                MdibStateModifications.Alert(updatedStates)
            }
            is MdibStateModifications.Component -> {
                val updatedStates = modifications.componentStates
                    .map { processStateUpdateSingleState(storage, it) }
                    .toList()
                MdibStateModifications.Component(updatedStates)
            }
            is MdibStateModifications.Metric -> {
                val updatedStates = modifications.metricStates
                    .map { processStateUpdateSingleState(storage, it) }
                    .toList()
                MdibStateModifications.Metric(updatedStates)
            }
            is MdibStateModifications.Operation -> {
                val updatedStates = modifications.operationStates
                    .map { processStateUpdateSingleState(storage, it) }
                    .toList()
                MdibStateModifications.Operation(updatedStates)
            }
            is MdibStateModifications.Waveform -> {
                val updatedStates = modifications.waveformStates
                    .map { processStateUpdateSingleState(storage, it) }
                    .toList()
                MdibStateModifications.Waveform(updatedStates)
            }
            is MdibStateModifications.Context -> {
                val updatedStates = modifications.contextStates
                    .map { processStateUpdateContextState(storage, it) }
                    .toList()
                MdibStateModifications.Context(updatedStates)
            }
        }
    }


    private fun processInsert(
        storage: MdibStorageRead,
        pair: Pair,
        parent: String?
    ): List<MdibDescriptionModification> {
        val updateResult: Pair = when (pair) {
            is Pair.SingleStatePair -> {
                processInsertSingleState(storage, pair)
            }
            is Pair.MultiStatePair -> {
                processInsertMultiState(storage, pair)
            }
        }

        interior.setUpdated(updateResult)
        val update: MutableList<MdibDescriptionModification> = parent?.let { parentHandle ->
            when (val parentEntity = storage.getEntity(parentHandle).getOrNull()) {
                null -> {
                    when (interior.isUpdatedParents(parentHandle)) {
                        false -> throw RuntimeException("Parent $parentHandle for newly inserted descriptor ${updateResult.handle} missing")
                        true ->  {
                            mutableListOf()
                        }
                    }
                }
                else -> processUpdate(
                    storage,
                    when (parentEntity.descriptor) {
                        is AbstractContextDescriptor -> Pair.tryFromThrowing(parentEntity.descriptor, parentEntity.getStates(AbstractMultiState::class.java))
                        else -> Pair.tryFromThrowing(parentEntity.descriptor, parentEntity.states[0])
                    }
                ).toMutableList()
            }
        } ?: mutableListOf()

        val newInsert = MdibDescriptionModification.Insert(updateResult, parent)
        update.add(0, newInsert) // prepend insert to update
        return update
    }

    private fun processUpdateAlreadyUpdatedMultiState(storage: MdibStorageRead, pair: Pair.MultiStatePair): Pair {
        val newVersion = checkNotNull(interior.getSavedVersionDescriptor(pair.handle)) {
            "Do not call this when no updated version is present"
        }

        val newPair = pair.copy()
        // update descriptor
        val newDescriptor = pair.pair.abstractDescriptor.copyTyped().also { it.descriptorVersion = newVersion.number }
        // update states
        val newStates: List<AbstractContextState> = newPair.pair.abstractContextStates
            .map { updateContextState(storage, newPair.descriptor.descriptorVersion, it.copyTyped()) }
            .toList()

        return Pair.tryFromThrowing(newDescriptor, newStates)
    }

    private fun processUpdateAlreadyUpdatedSingleState(pair: Pair.SingleStatePair): Pair {
        val newVersion = checkNotNull(interior.getSavedVersionPair(pair.handle)) {
            "Do not call this when no updated version is present"
        }

        val updatedDescriptor = pair.pair.abstractDescriptor.copyTyped()
            .also { it.descriptorVersion = newVersion.descriptorVersion }
        val updatedState = pair.pair.abstractState.copyTyped()
            .also { it.descriptorVersion = newVersion.descriptorVersion }
            .also { it.stateVersion = newVersion.stateVersion }

        return Pair.tryFromThrowing(updatedDescriptor, updatedState)
    }

    private fun processMultiStateUpdate(storage: MdibStorageRead, pair: Pair.MultiStatePair): Pair {
        val newDescriptorVersion = interior.getVersionDescriptor(storage, pair.handle) ?:
            throw RuntimeException("Could not determine the previous version for ${pair.handle}")

        val updatedDescriptor = pair.pair.abstractDescriptor.copyTyped()
            .also { it.descriptorVersion = newDescriptorVersion.inc().number }

        // collect already present states
        val storageStates: MutableMap<String, AbstractContextState> = storage.getContextStates(pair.handle)
            .associateBy { it.handle }
            .toMutableMap()

        val replaceVersions: (AbstractContextState) -> AbstractContextState = { state: AbstractContextState ->
            state.copyTyped()
                .also { it.descriptorVersion = updatedDescriptor.descriptorVersion }
                .also { it.stateVersion =
                    (interior.getVersionContextState(storage, it.handle) ?: Version.default())
                        .inc()
                        .number
                }
        }

        // increment versions for all states from change set
        val updatedStates: MutableList<AbstractContextState> = pair.pair
            .abstractContextStates
            .map {
                storageStates.remove(it.handle)
                replaceVersions(it)
            }
            .toMutableList()
        // update states not in change set
        storageStates
            .values
            .map { replaceVersions(it) }
            .forEach { updatedStates.add(it) }

        return Pair.tryFromThrowing(updatedDescriptor, updatedStates)
    }

    private fun processSingleStateUpdate(storage: MdibStorageRead, pair: Pair.SingleStatePair): Pair {
        val versionPair = interior.getVersionPair(storage, pair.handle) ?:
            throw RuntimeException("Could not determine the previous version for ${pair.handle}")

        val updatedDescriptor = pair.pair.abstractDescriptor.copyTyped()
            .also { it.descriptorVersion = versionPair.incDesc().descriptorVersion }

        val updatedState = pair.pair.abstractState.copyTyped()
            .also { it.descriptorVersion = updatedDescriptor.descriptorVersion }
            .also { it.stateVersion = versionPair.incState().stateVersion }

        return Pair.tryFromThrowing(updatedDescriptor, updatedState)
    }

    private fun processUpdate(storage: MdibStorageRead, pair: Pair): List<MdibDescriptionModification> {
        // we've already established new versions for at least this descriptor, handle them and throw them back.
        if (interior.getUpdatedVersions(pair.handle) != null) {
            val update = when (pair) {
                is Pair.SingleStatePair -> processUpdateAlreadyUpdatedSingleState(pair)
                is Pair.MultiStatePair -> processUpdateAlreadyUpdatedMultiState(storage, pair)
            }
            interior.setUpdated(update)
            return listOf(MdibDescriptionModification.Update(update))
        }

        val updated: Pair = when (pair) {
            is Pair.SingleStatePair -> processSingleStateUpdate(storage, pair)
            is Pair.MultiStatePair -> processMultiStateUpdate(storage, pair)
        }
        interior.setUpdated(updated)
        return listOf(MdibDescriptionModification.Update(updated))
    }

    private fun processDelete(storage: MdibStorageRead, handle: String): List<MdibDescriptionModification> {
        val entityToDelete = checkNotNull(storage.getEntity(handle).getOrNull()) {
            "Delete received not present handle $handle"
        }

        interior.setVersionOfDeletedArtifacts(entityToDelete.descriptor.handle, VersionPair.fromEntity(entityToDelete))

        entityToDelete.states
            .filterIsInstance<AbstractContextState>()
            .forEach { interior.setVersionOfDeletedArtifacts(it.handle, VersionPair.fromContext(it)) }

        // if mds, no incrementing parents needed
        if (entityToDelete.descriptor is MdsDescriptor) {
            return listOf(MdibDescriptionModification.Delete(handle))
        }

        val parentHandle = checkNotNull(entityToDelete.parent.getOrNull()) { "Non mds $handle had no parent" }

        // check if parent is getting deleted as well, don't add an update then
        if (interior.getDeletedHandles().contains(parentHandle)) {
            return listOf(MdibDescriptionModification.Delete(handle))
        }

        val parentEntity = checkNotNull(storage.getEntity(parentHandle).getOrNull()) {
            "Storage inconsistency, parent $parentHandle missing"
        }

        val update = processUpdate(
            storage,
            when (parentEntity.descriptor is AbstractContextDescriptor) {
                true -> Pair.tryFromThrowing(parentEntity.descriptor, parentEntity.getStates(AbstractContextState::class.java))
                false -> Pair.tryFromThrowing(parentEntity.descriptor, parentEntity.states[0])
            }
        ).toMutableList()

        update.add(0, MdibDescriptionModification.Delete(handle))
        return update
    }

    private fun updateContextState(storage: MdibStorageRead, descriptorVersion: BigInteger?, state: AbstractContextState): AbstractContextState {
        val stateVersion = interior.getSavedVersionState(state.handle) ?:
            (interior.getVersionContextState(storage, state.handle) ?: Version.default()).inc()

        return state.copyTyped()
            .also { it.descriptorVersion = descriptorVersion }
            .also { it.stateVersion = stateVersion.number }
    }

    private fun processInsertMultiState(storage: MdibStorageRead, pair: Pair.MultiStatePair): Pair {
        val descriptorVersion = (interior.getVersionDescriptor(storage, pair.handle) ?: Version.default()).inc()

        val updatedDescriptor = pair.pair.abstractDescriptor.copyTyped().also { it.descriptorVersion = descriptorVersion.number }
        val updatedStates = pair.pair.abstractContextStates
            .map { updateContextState(storage, updatedDescriptor.descriptorVersion, it) }
            .toList()

        return Pair.tryFromThrowing(updatedDescriptor, updatedStates)
    }

    private fun processInsertSingleState(storage: MdibStorageRead, pair: Pair.SingleStatePair): Pair {
        val (descriptorVersion, stateVersion) = interior.getSavedVersionPair(pair.handle)
            ?.let { Pair(it.descriptorVersion, it.stateVersion) }
            ?: (interior.getVersionPair(storage, pair.handle) ?: VersionPair.default())
                .let { Pair(it.incDesc().descriptorVersion, it.incState().stateVersion) }

        val updatedDescriptor = pair.pair.abstractDescriptor.copyTyped().also { it.descriptorVersion = descriptorVersion }
        val updatedState = pair.pair.abstractState
            .also { it.descriptorVersion = descriptorVersion }
            .also { it.stateVersion = stateVersion }

        return Pair.tryFromThrowing(updatedDescriptor, updatedState)
    }

    private fun <S: AbstractState> processStateUpdateSingleState(storage: MdibStorageRead, state: S): S {
        val versionPair = interior.getSavedVersionPair(state.descriptorHandle)
            ?: interior.getVersionPair(storage, state.descriptorHandle)?.incState()
            ?: throw RuntimeException("Could not determine the previous or saved version for state ${state.descriptorHandle}")

        return state.copyTyped()
            .also { it.descriptorVersion = versionPair.descriptorVersion }
            .also { it.stateVersion = versionPair.stateVersion }
    }

    private fun <S: AbstractContextState> processStateUpdateContextState(storage: MdibStorageRead, state: S): S {
        val versionPair = interior.getSavedVersionPair(state.handle)
            // if we find a present state, increment the state version
            ?: interior.getVersionPairContext(storage, state.handle)?.incState()
            // if neither a storage version, deleted entity version or saved version is present, this state is new
            ?: checkNotNull(interior.getVersionPair(storage, state.descriptorHandle)) {
                "Encountered context state ${state.handle} with unknown descriptor ${state.descriptorHandle}"
            }.let {
                VersionPair(it.descriptorVersion, null)
            }

        return state.copyTyped()
            .also { it.descriptorVersion = versionPair.descriptorVersion }
            .also { it.stateVersion = versionPair.stateVersion }
    }
}