package org.somda.sdc.biceps.provider.preprocessing

import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead
import kotlin.jvm.optionals.getOrNull

/**
 * Preprocessing segment which checks for descriptor handle collisions with already present descriptors and context states.
 */
class DuplicateDescriptorChecker: DescriptionPreprocessingSegment {
    override fun process(
        modifications: MutableList<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): MutableList<MdibDescriptionModification> {
        val duplicates = modifications
            .filterIsInstance<MdibDescriptionModification.Insert>()
            .mapNotNull {
                mdibStorage.getDescriptor(it.descriptorHandle).getOrNull()?.handle
                    ?: mdibStorage.getContextStates().find { state -> state.handle == it.descriptorHandle }?.handle
            }
            .toList()

        if (duplicates.isNotEmpty()) {
            throw HandleDuplicatedException("Inserted handles $duplicates are already present in MDIB")
        }
        return modifications
    }

}