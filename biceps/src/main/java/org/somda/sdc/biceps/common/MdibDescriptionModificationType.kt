package org.somda.sdc.biceps.common

/**
 * Designates the MDIB description modification type, i.e., insert, update, or delete.
 */
enum class MdibDescriptionModificationType {
    INSERT,
    UPDATE,
    DELETE
}