/**
 * Participant model factories.
 */
@DefaultQualifier(value = Nonnull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.biceps.model.participant.factory;

import jakarta.annotation.Nonnull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
