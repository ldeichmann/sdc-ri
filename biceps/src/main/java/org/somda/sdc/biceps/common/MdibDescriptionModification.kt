package org.somda.sdc.biceps.common

/**
 * A description modification containing the insert, updated or deleted descriptor as well as the state(s).
 *
 * Modifications are represented using a type-safe [Pair] that only permits valid combinations of
 * descriptors and states to be written to an MDIB.
 */
sealed class MdibDescriptionModification {
    /**
     * Represents an insert operation for modifying the MDIB.
     *
     * @property pair type-safe [Pair] to be written
     * @property parentHandle the optional parent element of this insert. Except for [SingleStatePair.Mds], all pairs
     * must have a parent handle.
     */
    data class Insert(
        val pair: Pair,
        val parentHandle: String?
    ): MdibDescriptionModification()

    /**
     * Represents an update operation for modifying the MDIB.
     *
     * @property pair type-safe [Pair] to be written
     */
    data class Update(
        val pair: Pair
    ): MdibDescriptionModification();

    /**
     * Represents a delete operation for removing an element from the MDIB.
     *
     * @property handle handle of the entity to delete from the MDIB
     */
    data class Delete(val handle: String): MdibDescriptionModification()

    val descriptorHandle: String
        get() = when (this) {
            is Insert -> this.pair.handle
            is Delete -> this.handle
            is Update -> this.pair.handle
        }

    val parent: String?
        get() = when (this) {
            is Insert -> this.parentHandle
            else -> null
        }
}

