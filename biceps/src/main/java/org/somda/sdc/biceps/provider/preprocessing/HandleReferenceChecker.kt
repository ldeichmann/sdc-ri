package org.somda.sdc.biceps.provider.preprocessing

import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead

/**
 * Preprocessing segment that checks that each state has a descriptor handle it references to states.
 */
class HandleReferenceChecker: DescriptionPreprocessingSegment {
    override fun process(
        modifications: MutableList<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): MutableList<MdibDescriptionModification> {
        modifications.forEach {
            when (it) {
                is MdibDescriptionModification.Delete -> {}
                is MdibDescriptionModification.Insert -> it.pair.states.forEach { state -> checkNotNull(state.descriptorHandle) { "State $state does not have a descriptor handle reference" } }
                is MdibDescriptionModification.Update -> it.pair.states.forEach { state -> checkNotNull(state.descriptorHandle) { "State $state does not have a descriptor handle reference" } }
            }
        }

        return modifications
    }
}