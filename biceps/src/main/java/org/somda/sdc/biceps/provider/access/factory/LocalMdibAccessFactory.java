package org.somda.sdc.biceps.provider.access.factory;

import org.somda.sdc.biceps.common.CommonConfig;
import org.somda.sdc.biceps.provider.access.LocalMdibAccess;

/**
 * Factory to create {@linkplain LocalMdibAccess} instances.
 */
public interface LocalMdibAccessFactory {
    /**
     * Creates a local mdib access with an MDIB version that has a random sequence id.
     * <p>
     * The preprocessing steps are visited in the order configured through
     * {@link CommonConfig#PROVIDER_DESCRIPTION_PREPROCESSING_SEGMENTS} and
     * {@link CommonConfig#PROVIDER_STATE_PREPROCESSING_SEGMENTS}
     *
     * @return the local mdib access instance.
     */
    LocalMdibAccess createLocalMdibAccess();
}
