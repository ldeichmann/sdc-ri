package org.somda.sdc.biceps.common

import org.somda.sdc.biceps.model.participant.AbstractAlertState
import org.somda.sdc.biceps.model.participant.AbstractContextState
import org.somda.sdc.biceps.model.participant.AbstractDeviceComponentState
import org.somda.sdc.biceps.model.participant.AbstractMetricState
import org.somda.sdc.biceps.model.participant.AbstractOperationState
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricState

/**
 * Describes a set of modified states in an MDIB.
 *
 * The states are grouped by the type of report they will result in to restrict modifications
 * which would violate BICEPS.
 */
sealed class WrittenMdibStateModifications {
    data class Alert(val alertStates: Map<String, List<AbstractAlertState>>): WrittenMdibStateModifications()
    data class Component(val componentStates: Map<String, List<AbstractDeviceComponentState>>): WrittenMdibStateModifications()
    data class Context(val contextStates: Map<String, List<AbstractContextState>>): WrittenMdibStateModifications()
    data class Metric(val metricStates: Map<String, List<AbstractMetricState>>): WrittenMdibStateModifications()
    data class Operation(val operationStates: Map<String, List<AbstractOperationState>>): WrittenMdibStateModifications()
    data class Waveform(val waveformStates: Map<String, List<RealTimeSampleArrayMetricState>>): WrittenMdibStateModifications()
}