package org.somda.sdc.biceps.common.event;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.WrittenMdibStateModifications;
import org.somda.sdc.biceps.common.access.MdibAccess;
import org.somda.sdc.common.CommonConfig;
import org.somda.sdc.common.event.EventBus;
import org.somda.sdc.common.logging.InstanceLogger;

import java.util.List;

/**
 * Utility class to distribute any BICEPS MDIB events.
 */
public class Distributor {
    private static final Logger LOG = LogManager.getLogger(Distributor.class);

    private final EventBus eventBus;
    private final Logger instanceLogger;

    @Inject
    Distributor(EventBus eventBus,
                @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier) {
        this.instanceLogger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier);
        this.eventBus = eventBus;
    }

    /**
     * Registers an observer to MDIB modification events.
     *
     * @param observer to unregister
     */
    public void registerObserver(Object observer) {
        eventBus.register(observer);
    }

    /**
     * Unregisters an observer from MDIB modification events.
     *
     * @param observer to unregister
     */
    public void unregisterObserver(Object observer) {
        eventBus.unregister(observer);
    }

    /**
     * Unregisters all observers at once.
     */
    public void unregisterAllObservers() {
        eventBus.unregisterAll();
    }

    /**
     * Creates a {@linkplain DescriptionModificationMessage} and sends it to all subscribers.
     *
     * @param mdibAccess       the MDIB access for {@link AbstractMdibAccessMessage}.
     * @param insertedEntities all inserted entities.
     * @param updatedEntities  all updated entities.
     * @param deletedEntities  all deleted entities.
     */
    public void sendDescriptionModificationEvent(MdibAccess mdibAccess,
                                                 List<MdibEntity> insertedEntities,
                                                 List<MdibEntity> updatedEntities,
                                                 List<MdibEntity> deletedEntities) {
        eventBus.post(
                new DescriptionModificationMessage(mdibAccess, insertedEntities, updatedEntities, deletedEntities)
        );
    }

    /**
     * Creates a specific {@linkplain StateModificationMessage} based on the change type and
     * sends it to all subscribers.
     *
     * @param mdibAccess    the MDIB access for {@link AbstractMdibAccessMessage}.
     * @param modifications the changes type that were written to the mdib
     */
    public void sendStateModificationEvent(
            MdibAccess mdibAccess,
            WrittenMdibStateModifications modifications
    ) {

        AbstractMdibAccessMessage accessMessage;
        if (modifications instanceof WrittenMdibStateModifications.Alert) {
            accessMessage = new AlertStateModificationMessage(
                    mdibAccess,
                    ((WrittenMdibStateModifications.Alert) modifications).getAlertStates()
            );
        } else if (modifications instanceof WrittenMdibStateModifications.Component) {
            accessMessage = new ComponentStateModificationMessage(
                    mdibAccess,
                    ((WrittenMdibStateModifications.Component) modifications).getComponentStates()
            );
        } else if (modifications instanceof WrittenMdibStateModifications.Context) {
            accessMessage = new ContextStateModificationMessage(
                    mdibAccess,
                    ((WrittenMdibStateModifications.Context) modifications).getContextStates()
            );
        } else if (modifications instanceof WrittenMdibStateModifications.Metric) {
            accessMessage = new MetricStateModificationMessage(
                    mdibAccess,
                    ((WrittenMdibStateModifications.Metric) modifications).getMetricStates()
            );
        } else if (modifications instanceof WrittenMdibStateModifications.Operation) {
            accessMessage = new OperationStateModificationMessage(
                    mdibAccess,
                    ((WrittenMdibStateModifications.Operation) modifications).getOperationStates()
            );
        } else if (modifications instanceof WrittenMdibStateModifications.Waveform) {
            accessMessage = new WaveformStateModificationMessage(
                    mdibAccess,
                    ((WrittenMdibStateModifications.Waveform) modifications).getWaveformStates()
            );
        } else {
            instanceLogger.error(
                    "Expected known modification type modification message. Distribution failed."
            );
            return;
        }

        try {
            eventBus.post(accessMessage);
        } catch (IllegalArgumentException e) {
            instanceLogger.error("Failed to call state event message constructor", e);
        }
    }
}
