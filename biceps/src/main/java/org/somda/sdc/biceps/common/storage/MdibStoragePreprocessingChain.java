package org.somda.sdc.biceps.common.storage;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibStateModifications;

import java.util.List;

/**
 * Provides a processing chain that is supposed to be run before any interaction with
 * an {@linkplain MdibStorage} instance.
 * <p>
 * The {@linkplain MdibStoragePreprocessingChain} offers processing functions for description and state change sets.
 */
public class MdibStoragePreprocessingChain {
    private final MdibStorage mdibStorage;
    private final List<DescriptionPreprocessingSegment> descriptionChainSegments;
    private final List<StatePreprocessingSegment> stateChainSegments;

    @Inject
    MdibStoragePreprocessingChain(@Assisted MdibStorage mdibStorage,
                                  @Assisted List<DescriptionPreprocessingSegment> descriptionChainSegments,
                                  @Assisted List<StatePreprocessingSegment> stateChainSegments) {
        this.mdibStorage = mdibStorage;
        this.descriptionChainSegments = descriptionChainSegments;
        this.stateChainSegments = stateChainSegments;
    }

    /**
     * Accepts a set of description modifications and applies them on every available description chain segment.
     *
     * @param modifications the modification to pass to the chain segments.
     * @throws PreprocessingException in case a chain segment fails.
     * @return processed description modifications
     */
    public MdibDescriptionModifications processDescriptionModifications(
        MdibDescriptionModifications modifications
    ) throws PreprocessingException {

        var preprocessed = modifications.asList();
        for (var chainSegment : descriptionChainSegments) {
            preprocessed = chainSegment.beforeFirstModification(preprocessed, mdibStorage);
        }

        var processed = preprocessed;
        for (var chainSegment : descriptionChainSegments) {
            try {
                processed = chainSegment.process(processed, mdibStorage);
                // CHECKSTYLE.OFF: IllegalCatch
            } catch (Exception e) {
                // CHECKSTYLE.ON: IllegalCatch
                throw new PreprocessingException(e.getMessage(), e, chainSegment.toString());
            }
        }

        var postprocessed = processed;
        for (var chainSegment : descriptionChainSegments) {
            postprocessed = chainSegment.afterLastModification(postprocessed, mdibStorage);
        }

        final var updated = new MdibDescriptionModifications();
        updated.addAll(postprocessed);
        return updated;
    }

    /**
     * Accepts a set of state modifications and applies them on every available state chain segment.
     *
     * @param modifications the modification to pass to the chain segments.
     * @throws PreprocessingException in case a chain segment fails.
     * @return processed state modifications
     */
    public MdibStateModifications processStateModifications(
        MdibStateModifications modifications
    ) throws PreprocessingException {
        var preprocessed = modifications;
        for (var chainSegment : stateChainSegments) {
            preprocessed = chainSegment.beforeFirstModification(preprocessed, mdibStorage);
        }

        var processed = preprocessed;
        for (var chainSegment : stateChainSegments) {
            try {
                processed = chainSegment.process(processed, mdibStorage);
                // CHECKSTYLE.OFF: IllegalCatch
            } catch (Exception e) {
                // CHECKSTYLE.ON: IllegalCatch
                throw new PreprocessingException(e.getMessage(), e.getCause(), chainSegment.toString());
            }
        }

        var postprocessed = processed;
        for (var chainSegment : stateChainSegments) {
            postprocessed = chainSegment.afterLastModification(postprocessed, mdibStorage);
        }
        return postprocessed;
    }
}
