package org.somda.sdc.biceps.provider.preprocessing

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertInstanceOf
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.somda.sdc.biceps.UnitTestUtil
import org.somda.sdc.biceps.common.CommonConfig
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.MdibTypeValidator
import org.somda.sdc.biceps.common.MultiStatePair
import org.somda.sdc.biceps.common.Pair
import org.somda.sdc.biceps.common.SingleStatePair
import org.somda.sdc.biceps.common.storage.MdibStorage
import org.somda.sdc.biceps.common.storage.factory.MdibStorageFactory
import org.somda.sdc.biceps.guice.DefaultBicepsConfigModule
import org.somda.sdc.biceps.model.participant.AbstractState
import org.somda.sdc.biceps.model.participant.ChannelDescriptor
import org.somda.sdc.biceps.model.participant.ChannelState
import org.somda.sdc.biceps.model.participant.ClockDescriptor
import org.somda.sdc.biceps.model.participant.ClockState
import org.somda.sdc.biceps.model.participant.MdibVersion
import org.somda.sdc.biceps.model.participant.MeansContextDescriptor
import org.somda.sdc.biceps.model.participant.MeansContextState
import org.somda.sdc.biceps.model.participant.MetricAvailability
import org.somda.sdc.biceps.model.participant.MetricCategory
import org.somda.sdc.biceps.model.participant.NumericMetricDescriptor
import org.somda.sdc.biceps.model.participant.NumericMetricState
import org.somda.sdc.biceps.model.participant.OperatingJurisdiction
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor
import org.somda.sdc.biceps.model.participant.PatientContextState
import org.somda.sdc.biceps.model.participant.SafetyClassification
import org.somda.sdc.biceps.model.participant.SystemContextState
import org.somda.sdc.biceps.model.participant.VmdDescriptor
import org.somda.sdc.biceps.model.participant.VmdState
import org.somda.sdc.biceps.model.participant.factory.CodedValueFactory
import org.somda.sdc.biceps.testutil.BaseTreeModificationsSet
import org.somda.sdc.biceps.testutil.Handles
import org.somda.sdc.biceps.testutil.MockEntryFactory
import org.somda.sdc.common.util.copyTyped
import test.org.somda.common.LoggingTestWatcher
import java.math.BigDecimal
import java.math.BigInteger
import kotlin.jvm.optionals.getOrNull

@ExtendWith(LoggingTestWatcher::class)
class VersionHandlerTest {

    private val unitTestUtil = UnitTestUtil(object : DefaultBicepsConfigModule() {
        override fun customConfigure() {
            // Configure to avoid copying and make comparison easier
            bind(
                CommonConfig.COPY_MDIB_OUTPUT,
                Boolean::class.java,
                false
            )
        }
    })

    private lateinit var mdibStorage: MdibStorage
    private lateinit var versionHandler: VersionHandler

    private val mdibTypeValidator = unitTestUtil.injector.getInstance(MdibTypeValidator::class.java)
    private val mockEntryFactory = MockEntryFactory(mdibTypeValidator)

    @BeforeEach
    fun beforeEach() {
        // Given a version handler and sample input
        mdibStorage = unitTestUtil.injector.getInstance(MdibStorageFactory::class.java)
            .createMdibStorage()
        versionHandler = unitTestUtil.injector.getInstance(VersionHandler::class.java)

        mdibStorage.apply(
            MdibVersion.create(),
            BigInteger.ZERO,
            BigInteger.ZERO,
            BaseTreeModificationsSet(mockEntryFactory).createBaseTree()
        )
    }

    /**
     * Inserts a single state descriptor and checks that the parent is updated.
     */
    @Test
    fun `descriptor versioning single state insert causes update`() {
        baseStorage()
        var modifications = listOf<MdibDescriptionModification>(
            MdibDescriptionModification.Insert(
                mockEntryFactory.entry(Handles.CHANNEL_3, ChannelDescriptor::class.java, ChannelState::class.java),
                Handles.VMD_0
            )
        )

        val parentEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        // update added
        assertEquals(2, modifications.size)

        val update = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])
        val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, update.pair)
        val updatedVmd = assertInstanceOf(SingleStatePair.Vmd::class.java, singleStatePair.pair)

        val expectedNewDescriptorVersion = parentEntity.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
        assertEquals(expectedNewDescriptorVersion, updatedVmd.descriptor.descriptorVersion)
        assertEquals(expectedNewDescriptorVersion, updatedVmd.state.descriptorVersion)

        assertEquals(
            parentEntity.getFirstState(VmdState::class.java).getOrNull()!!.stateVersion!!.plus(BigInteger.ONE),
            updatedVmd.state.stateVersion
        )

        // make sure update is in cache and removed after the before_first_modification call
        val interior = getInterior(versionHandler)

        assertNotNull(interior.getUpdatedVersions(Handles.VMD_0)) { "Cache does not contain ${Handles.VMD_0}" }
        versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertNull(interior.getUpdatedVersions(Handles.VMD_0)) { "Cache still contains ${Handles.VMD_0}" }
    }

    /**
     * Inserts a multi state descriptor with no states and checks that the parent is updated.
     */
    @Test
    fun `descriptor versioning multi state without states insert causes update`() {
        baseStorage()

        var modifications = listOf<MdibDescriptionModification>(MdibDescriptionModification.Insert(
            mockEntryFactory.contextEntry(
                Handles.CONTEXTDESCRIPTOR_5,
                emptyList<String>(),
                MeansContextDescriptor::class.java,
                MeansContextState::class.java
            ),
            Handles.SYSTEMCONTEXT_0
        ))

        val parentEntity = mdibStorage.getEntity(Handles.SYSTEMCONTEXT_0).orElseThrow()

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(2, modifications.size)

        val update = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])
        val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, update.pair)
        val updatedSystemContext = assertInstanceOf(SingleStatePair.SystemContext::class.java, singleStatePair.pair)

        val expectedNewDescriptorVersion = parentEntity.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
        assertEquals(expectedNewDescriptorVersion, updatedSystemContext.descriptor.descriptorVersion)
        assertEquals(expectedNewDescriptorVersion, updatedSystemContext.state.descriptorVersion)

        assertEquals(
            parentEntity.getFirstState(SystemContextState::class.java).getOrNull()!!.stateVersion!!.plus(BigInteger.ONE),
            updatedSystemContext.state.stateVersion
        )
    }

    /**
     * Inserts a multi state descriptor with states and checks that the parent is updated.
     */
    @Test
    fun `descriptor versioning multi state insert causes update`() {
        baseStorage()

        var modifications = listOf<MdibDescriptionModification>(MdibDescriptionModification.Insert(
            mockEntryFactory.contextEntry(
                Handles.CONTEXTDESCRIPTOR_5,
                Handles.CONTEXT_6,
                MeansContextDescriptor::class.java,
                MeansContextState::class.java
            ),
            Handles.SYSTEMCONTEXT_0
        ))

        val parentEntity = mdibStorage.getEntity(Handles.SYSTEMCONTEXT_0).orElseThrow()

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(2, modifications.size)

        val update = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])
        val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, update.pair)
        val updatedSystemContext = assertInstanceOf(SingleStatePair.SystemContext::class.java, singleStatePair.pair)

        val expectedNewDescriptorVersion = parentEntity.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
        assertEquals(expectedNewDescriptorVersion, updatedSystemContext.descriptor.descriptorVersion)
        assertEquals(expectedNewDescriptorVersion, updatedSystemContext.state.descriptorVersion)

        assertEquals(
            parentEntity.getFirstState(SystemContextState::class.java).getOrNull()!!.stateVersion!!.plus(BigInteger.ONE),
            updatedSystemContext.state.stateVersion
        )
    }

    /**
     * Inserts a single state descriptor, checks that the parent is updated, but also the manually added update later on.
     */
    @Test
    fun `descriptor versioning single state insert updates later update versions`() {
        baseStorage()

        val initialModifications = mutableListOf<MdibDescriptionModification>(
            MdibDescriptionModification.Insert(
                mockEntryFactory.entry(Handles.CHANNEL_3, ChannelDescriptor::class.java, ChannelState::class.java),
                Handles.VMD_0
            )
        )

        val parentEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()

        // add an update for the vmd as well
        val vmdDescriptor = parentEntity.getDescriptor(VmdDescriptor::class.java).getOrNull()!!.copyTyped()
            .also { it.safetyClassification = SafetyClassification.MED_C }
        val vmdState = parentEntity.getFirstState(VmdState::class.java).getOrNull()!!.copyTyped()

        val updatedVmdDescriptor = vmdDescriptor.copyTyped()

        initialModifications.add(
            MdibDescriptionModification.Update(
                Pair.tryFromThrowing(updatedVmdDescriptor, vmdState.copyTyped())
            )
        )

        var modifications = versionHandler.beforeFirstModification(initialModifications, mdibStorage)
        assertEquals(2, modifications.size)

        // another update added
        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(3, modifications.size)

        val firstUpdate = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])
        val secondUpdate = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[2])

        // make sure update bumps parent version but does not add safety classification
        run {
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, firstUpdate.pair)
            val updatedVmd = assertInstanceOf(SingleStatePair.Vmd::class.java, singleStatePair.pair)

            val expectedNewDescriptorVersion = parentEntity.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
            assertEquals(expectedNewDescriptorVersion, updatedVmd.descriptor.descriptorVersion)
            assertEquals(expectedNewDescriptorVersion, updatedVmd.state.descriptorVersion)
            assertNull(updatedVmd.descriptor.safetyClassification)

            assertEquals(
                incOptionalBigInteger(parentEntity.getFirstState(VmdState::class.java).getOrNull()!!.stateVersion),
                updatedVmd.state.stateVersion
            )
        }

        // make sure manual update bumps parent version and sets safety classification
        run {
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, secondUpdate.pair)
            val updatedVmd = assertInstanceOf(SingleStatePair.Vmd::class.java, singleStatePair.pair)

            val expectedNewDescriptorVersion = parentEntity.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
            assertEquals(expectedNewDescriptorVersion, updatedVmd.descriptor.descriptorVersion)
            assertEquals(expectedNewDescriptorVersion, updatedVmd.state.descriptorVersion)
            assertEquals(SafetyClassification.MED_C, updatedVmd.descriptor.safetyClassification)

            assertEquals(
                incOptionalBigInteger(parentEntity.getFirstState(VmdState::class.java).getOrNull()!!.stateVersion),
                updatedVmd.state.stateVersion
            )
        }
    }

    /**
     * Updates a multi state descriptor and adds a new state as a side effect, ensures versions are correctly updated,
     * i.e. only the new states is not incremented.
     */
    @Test
    fun `test descriptor versioning update insert new multi state state`() {
        baseStorage()

        val patientEntity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0).orElseThrow()

        val patientDescriptor = patientEntity.getDescriptor(PatientContextDescriptor::class.java).orElseThrow()
        val patientStates = patientEntity.getStates(PatientContextState::class.java)
        assertFalse(patientStates.isEmpty()) { "Test requires previously present states" }

        val newContextStates: MutableList<PatientContextState> = patientStates.map { it.copyTyped() }.toMutableList()
        newContextStates.addAll(
            mockEntryFactory
                .contextEntry(patientDescriptor.handle, Handles.CONTEXT_6, PatientContextDescriptor::class.java, PatientContextState::class.java)
                .states.map { it as PatientContextState }
        )

        var modifications = listOf<MdibDescriptionModification>(
            MdibDescriptionModification.Update(Pair.tryFromThrowing(patientDescriptor.copyTyped(), newContextStates))
        )

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        val update = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[0])
        val multiStatePair = assertInstanceOf(Pair.MultiStatePair::class.java, update.pair)
        val updatedPatient = assertInstanceOf(MultiStatePair.PatientContext::class.java, multiStatePair.pair)

        val expectedNewDescriptorVersion = patientDescriptor.descriptorVersion!!.plus(BigInteger.ONE)
        assertEquals(expectedNewDescriptorVersion, updatedPatient.descriptor.descriptorVersion)

        assertFalse(updatedPatient.state.isEmpty())

        updatedPatient.state.forEach { state ->
            when (state.handle) {
                Handles.CONTEXT_6 -> {
                    assertEquals(BigInteger.ZERO, state.stateVersion)
                }
                else -> {
                    val oldState = patientStates.find { it.handle == state.handle }!!
                    assertEquals(oldState.stateVersion!!.add(BigInteger.ONE), state.stateVersion)
                }
            }
            assertEquals(expectedNewDescriptorVersion, state.descriptorVersion)
        }
    }

    /**
     * Updates multiple single states with incorrect versions, checks for correctness of versions of all updates
     * after processing.
     */
    @Test
    fun `descriptor versioning updates`() {
        baseStorage()

        val vmdEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()
        val channelEntity = mdibStorage.getEntity(Handles.CHANNEL_0).orElseThrow()
        val metricEntity = mdibStorage.getEntity(Handles.METRIC_0).orElseThrow()

        val vmdPair = SingleStatePair.Vmd(
            vmdEntity.getDescriptor(VmdDescriptor::class.java).orElseThrow(),
            vmdEntity.getFirstState(VmdState::class.java).orElseThrow()
        )

        val channelPair = SingleStatePair.Channel(
            channelEntity.getDescriptor(ChannelDescriptor::class.java).orElseThrow(),
            channelEntity.getFirstState(ChannelState::class.java).orElseThrow()
        )

        val metricPair = SingleStatePair.NumericMetric(
            metricEntity.getDescriptor(NumericMetricDescriptor::class.java).orElseThrow(),
            metricEntity.getFirstState(NumericMetricState::class.java).orElseThrow()
        )

        // update a bunch of versions and check for correct versions later on
        var modifications = listOf<MdibDescriptionModification>(
            MdibDescriptionModification.Update(
                Pair.tryFromThrowing(
                    vmdPair.descriptor.copyTyped().also { it.descriptorVersion = BigInteger.valueOf(4) },
                    vmdPair.state.copyTyped()
                )
            ),
            MdibDescriptionModification.Update(
                Pair.tryFromThrowing(
                    channelPair.descriptor.copyTyped().also { it.descriptorVersion = BigInteger.valueOf(12) },
                    channelPair.state.copyTyped()
                )
            ),
            MdibDescriptionModification.Update(
                Pair.tryFromThrowing(
                    metricPair.descriptor.copyTyped().also { it.descriptorVersion = BigInteger.valueOf(5) },
                    metricPair.state.copyTyped().also { it.stateVersion = BigInteger.TEN }
                )
            ),
        )

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(3, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(3, modifications.size)

        val firstUpdate = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[0])
        val secondUpdate = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])
        val thirdUpdate = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[2])

        run {
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, firstUpdate.pair)
            val updatedVmd = assertInstanceOf(SingleStatePair.Vmd::class.java, singleStatePair.pair)

            val expectedDescriptorVersion = vmdPair.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
            assertEquals(expectedDescriptorVersion, updatedVmd.descriptor.descriptorVersion)
            assertEquals(expectedDescriptorVersion, updatedVmd.state.descriptorVersion)

            assertEquals(vmdPair.state.stateVersion!!.plus(BigInteger.ONE), updatedVmd.state.stateVersion)
        }

        run {
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, secondUpdate.pair)
            val updatedChannel = assertInstanceOf(SingleStatePair.Channel::class.java, singleStatePair.pair)

            val expectedDescriptorVersion = channelPair.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
            assertEquals(expectedDescriptorVersion, updatedChannel.descriptor.descriptorVersion)
            assertEquals(expectedDescriptorVersion, updatedChannel.state.descriptorVersion)

            assertEquals(channelPair.state.stateVersion!!.plus(BigInteger.ONE), updatedChannel.state.stateVersion)
        }

        run {
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, thirdUpdate.pair)
            val updatedMetric = assertInstanceOf(SingleStatePair.NumericMetric::class.java, singleStatePair.pair)

            val expectedDescriptorVersion = metricPair.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
            assertEquals(expectedDescriptorVersion, updatedMetric.descriptor.descriptorVersion)
            assertEquals(expectedDescriptorVersion, updatedMetric.state.descriptorVersion)

            assertEquals(metricPair.state.stateVersion!!.plus(BigInteger.ONE), updatedMetric.state.stateVersion)
        }
    }

    /**
     * Deletes a metric descriptor, checks for correct versioning of the parent.
     */
    @Test
    fun `descriptor delete causes update`() {
        baseStorage()

        val channelEntity = mdibStorage.getEntity(Handles.CHANNEL_0).orElseThrow()
        val metricEntity = mdibStorage.getEntity(Handles.METRIC_0).orElseThrow()

        val channelPair = SingleStatePair.Channel(
            channelEntity.getDescriptor(ChannelDescriptor::class.java).orElseThrow(),
            channelEntity.getFirstState(ChannelState::class.java).orElseThrow()
        )

        val metricPair = SingleStatePair.NumericMetric(
            metricEntity.getDescriptor(NumericMetricDescriptor::class.java).orElseThrow(),
            metricEntity.getFirstState(NumericMetricState::class.java).orElseThrow()
        )

        // delete metric
        var modifications = listOf<MdibDescriptionModification>(
            MdibDescriptionModification.Delete(metricPair.handle)
        )

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(2, modifications.size)

        assertInstanceOf(MdibDescriptionModification.Delete::class.java, modifications[0])
        val update = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])

        // check channel version
        val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, update.pair)
        val updatedChannel = assertInstanceOf(SingleStatePair.Channel::class.java, singleStatePair.pair)


        val expectedDescriptorVersion = channelPair.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
        assertEquals(expectedDescriptorVersion, updatedChannel.descriptor.descriptorVersion)
        assertEquals(expectedDescriptorVersion, updatedChannel.state.descriptorVersion)

        assertEquals(channelPair.state.stateVersion!!.inc(), updatedChannel.state.stateVersion)
    }

    /**
     * Deletes a channel and its metric, ensuring that deleting the metric does not cause a channel update.
     */
    @Test
    fun `descriptor delete subtree causes no additional updates`() {
        baseStorage()

        val vmdEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()
        val channelEntity = mdibStorage.getEntity(Handles.CHANNEL_0).orElseThrow()
        val metricEntity = mdibStorage.getEntity(Handles.METRIC_0).orElseThrow()

        val vmdPair = SingleStatePair.Vmd(
            vmdEntity.getDescriptor(VmdDescriptor::class.java).orElseThrow(),
            vmdEntity.getFirstState(VmdState::class.java).orElseThrow()
        )

        val channelPair = SingleStatePair.Channel(
            channelEntity.getDescriptor(ChannelDescriptor::class.java).orElseThrow(),
            channelEntity.getFirstState(ChannelState::class.java).orElseThrow()
        )

        val metricPair = SingleStatePair.NumericMetric(
            metricEntity.getDescriptor(NumericMetricDescriptor::class.java).orElseThrow(),
            metricEntity.getFirstState(NumericMetricState::class.java).orElseThrow()
        )

        // delete channel and metric
        run {
            var modifications = listOf<MdibDescriptionModification>(
                MdibDescriptionModification.Delete(metricPair.handle),
                MdibDescriptionModification.Delete(channelPair.handle),
            )

            modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
            assertEquals(2, modifications.size)

            modifications = versionHandler.process(modifications, mdibStorage)
            assertEquals(3, modifications.size)

            val firstDelete = assertInstanceOf(MdibDescriptionModification.Delete::class.java, modifications[0])
            val secondDelete = assertInstanceOf(MdibDescriptionModification.Delete::class.java, modifications[1])
            val update = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[2])

            assertEquals(metricPair.handle, firstDelete.handle)
            assertEquals(channelPair.handle, secondDelete.handle)

            // check vmd version
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, update.pair)
            val updatedVmd = assertInstanceOf(SingleStatePair.Vmd::class.java, singleStatePair.pair)

            val expectedDescriptorVersion = vmdPair.descriptor.descriptorVersion!!.plus(BigInteger.ONE)
            assertEquals(expectedDescriptorVersion, updatedVmd.descriptor.descriptorVersion)
            assertEquals(expectedDescriptorVersion, updatedVmd.state.descriptorVersion)

            assertEquals(vmdPair.state.stateVersion!!.inc(), updatedVmd.state.stateVersion)

        }
    }

    @Test
    fun `delete and reinsert increments descriptor version`() {
        baseStorage()

        val channelEntity = mdibStorage.getEntity(Handles.CHANNEL_0).orElseThrow()
        val metricEntity = mdibStorage.getEntity(Handles.METRIC_0).orElseThrow()

        val channelPair = SingleStatePair.Channel(
            channelEntity.getDescriptor(ChannelDescriptor::class.java).orElseThrow(),
            channelEntity.getFirstState(ChannelState::class.java).orElseThrow()
        )

        val metricPair = SingleStatePair.NumericMetric(
            metricEntity.getDescriptor(NumericMetricDescriptor::class.java).orElseThrow(),
            metricEntity.getFirstState(NumericMetricState::class.java).orElseThrow()
        )

        // delete metric
        run {
            var modifications = listOf<MdibDescriptionModification>(
                MdibDescriptionModification.Delete(metricPair.handle),
            )

            modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
            assertEquals(1, modifications.size)
            modifications = versionHandler.process(modifications, mdibStorage)
            assertEquals(2, modifications.size)
            modifications = versionHandler.afterLastModification(modifications, mdibStorage)
            assertEquals(2, modifications.size)

            mdibStorage.apply(
                MdibVersion.increment(mdibStorage.mdibVersion),
                mdibStorage.mdDescriptionVersion.inc(),
                mdibStorage.mdStateVersion.inc(),
                MdibDescriptionModifications().also { it.addAll(modifications) }
            )
        }
        // reinsert metric
        run {
            var modifications = listOf<MdibDescriptionModification>(
                MdibDescriptionModification.Insert(mockEntryFactory.entry(
                    Handles.METRIC_0,
                    NumericMetricDescriptor::class.java,
                    { t: NumericMetricDescriptor ->
                        t.resolution = BigDecimal.ONE
                        t.metricCategory = MetricCategory.UNSPEC
                        t.metricAvailability = MetricAvailability.INTR
                        t.unit = CodedValueFactory.createIeeeCodedValue("500")
                    },
                    { _: AbstractState? -> }), Handles.CHANNEL_0),
            )

            modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
            assertEquals(1, modifications.size)
            modifications = versionHandler.process(modifications, mdibStorage)
            assertEquals(2, modifications.size)
            modifications = versionHandler.afterLastModification(modifications, mdibStorage)
            assertEquals(2, modifications.size)

            val insert = assertInstanceOf(MdibDescriptionModification.Insert::class.java, modifications[0])

            assertEquals(channelPair.handle, insert.parentHandle)
            // check metric version
            val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, insert.pair)
            val insertedMetric = assertInstanceOf(SingleStatePair.NumericMetric::class.java, singleStatePair.pair)

            assertEquals(BigInteger.ONE, insertedMetric.descriptor.descriptorVersion)
            assertEquals(BigInteger.ONE, insertedMetric.state.descriptorVersion)
            assertEquals(BigInteger.ONE, insertedMetric.state.stateVersion)

            mdibStorage.apply(
                MdibVersion.increment(mdibStorage.mdibVersion),
                mdibStorage.mdDescriptionVersion.inc(),
                mdibStorage.mdStateVersion.inc(),
                MdibDescriptionModifications().also { it.addAll(modifications) }
            )
        }

    }

    @Test
    fun `state versioning component`() {
        baseStorage()

        val vmdEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()
        val clockEntity = mdibStorage.getEntity(Handles.CLOCK_0).orElseThrow()

        val vmdCode = CodedValueFactory.createCodedValue(null, null, "t0ps3cr3t")
        val clockBadVersion = BigInteger.valueOf(500)

        val vmdDescriptor = vmdEntity.getDescriptor(VmdDescriptor::class.java).orElseThrow()
        val vmdState = vmdEntity.getFirstState(VmdState::class.java).orElseThrow()

        val clockDescriptor = clockEntity.getDescriptor(ClockDescriptor::class.java).orElseThrow()
        val clockState = clockEntity.getFirstState(ClockState::class.java).orElseThrow()

        val vmdStateForUpdate = vmdState
            .copyTyped()
            .also { state ->
                state.operatingJurisdiction = OperatingJurisdiction()
                    .also { it.type = vmdCode }
            }

        val clockStateForUpdate = clockEntity.getFirstState(ClockState::class.java).orElseThrow()
            .copyTyped()
            .also { it.descriptorVersion = clockBadVersion }
            .also { it.stateVersion = clockBadVersion }

        val stateModification = MdibStateModifications.Component(listOf(vmdStateForUpdate, clockStateForUpdate))

        var modifications = versionHandler.beforeFirstModification(stateModification, mdibStorage)
        assertEquals(2, modifications.size)

        modifications = versionHandler.process(stateModification, mdibStorage)
        assertEquals(2, modifications.size)

        val updatedVmd = assertInstanceOf(VmdState::class.java, modifications.states[0])
        val updatedClock = assertInstanceOf(ClockState::class.java, modifications.states[1])

        assertEquals(vmdDescriptor.descriptorVersion, updatedVmd.descriptorVersion)
        assertEquals(vmdState.stateVersion!!.inc(), updatedVmd.stateVersion)
        assertEquals(vmdCode, updatedVmd.operatingJurisdiction!!.type)

        assertEquals(clockDescriptor.descriptorVersion, updatedClock.descriptorVersion)
        assertEquals(clockState.stateVersion!!.inc(), updatedClock.stateVersion)

        assertNotEquals(clockBadVersion, updatedClock.descriptorVersion)
        assertNotEquals(clockBadVersion, updatedClock.stateVersion)

        assertNotEquals(clockBadVersion.inc(), updatedClock.descriptorVersion)
        assertNotEquals(clockBadVersion.inc(), updatedClock.stateVersion)
    }

    @Test
    fun `state versioning component multiple`() {
        baseStorage()

        val vmdEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()

        val vmdDescriptor = vmdEntity.getDescriptor(VmdDescriptor::class.java).orElseThrow()
        val vmdState = vmdEntity.getFirstState(VmdState::class.java).orElseThrow()

        val vmdCode = CodedValueFactory.createCodedValue(null, null, "t0ps3cr3t")
        val badVersion = BigInteger.valueOf(500)

        val badVmdStateForUpdate = vmdState
            .copyTyped()
            .also { state ->
                state.operatingJurisdiction = OperatingJurisdiction()
                    .also { it.type = vmdCode }
            }
            .also { it.descriptorVersion = badVersion }
            .also { it.stateVersion = badVersion }

        val stateModification = MdibStateModifications.Component(listOf(badVmdStateForUpdate, badVmdStateForUpdate.copyTyped()))

        var modifications = versionHandler.beforeFirstModification(stateModification, mdibStorage)
        assertEquals(2, modifications.size)

        modifications = versionHandler.process(stateModification, mdibStorage)
        assertEquals(2, modifications.size)

        modifications.states.forEach { mod ->
            val vmd = assertInstanceOf(VmdState::class.java, mod)

            assertEquals(vmdDescriptor.descriptorVersion, vmd.descriptorVersion)
            assertEquals(vmdState.stateVersion!!.inc(), vmd.stateVersion)
            assertEquals(vmdCode, vmd.operatingJurisdiction!!.type)
        }
    }

    @Test
    fun `state versioning context`() {
        baseStorage()

        val patientEntity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0).orElseThrow()

        val bindingVersion = BigInteger.valueOf(5)
        val badVersion = BigInteger.valueOf(650)

        val firstState = assertInstanceOf(PatientContextState::class.java, patientEntity.getStates()[0])
            .copyTyped()
            .also { it.bindingMdibVersion = bindingVersion }
            .also { it.descriptorVersion = badVersion }
            .also { it.stateVersion = badVersion }

        val newState = assertInstanceOf(PatientContextState::class.java, mockEntryFactory.contextEntry(patientEntity.handle, Handles.CONTEXT_6, PatientContextDescriptor::class.java, PatientContextState::class.java).states[0])
            .also { it.descriptorVersion = badVersion }
            .also { it.stateVersion = badVersion }

        val stateModification = MdibStateModifications.Context(listOf(firstState, newState))

        var modifications = versionHandler.beforeFirstModification(stateModification, mdibStorage)
        assertEquals(2, modifications.size)

        modifications = versionHandler.process(stateModification, mdibStorage)
        assertEquals(2, modifications.size)

        run {
            val state = assertInstanceOf(PatientContextState::class.java, modifications.states[0])
            assertEquals(Handles.CONTEXT_0, state.handle)
            assertEquals(BigInteger.ONE, state.stateVersion)
            assertEquals(patientEntity.descriptor.descriptorVersion, state.descriptorVersion)
            assertEquals(bindingVersion, state.bindingMdibVersion)
        }

        run {
            val state = assertInstanceOf(PatientContextState::class.java, modifications.states[1])
            assertNull(state.stateVersion)
            assertEquals(Handles.CONTEXT_6, state.handle)
            assertEquals(patientEntity.descriptor.descriptorVersion, state.descriptorVersion)
        }
    }

    @Test
    fun `state versioning context multiple`() {
        baseStorage()

        val patientEntity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0).orElseThrow()
        val patientContextState = assertInstanceOf(PatientContextState::class.java, patientEntity.states[0])

        val bindingVersion = BigInteger.valueOf(5)
        val badVersion = BigInteger.valueOf(650)

        val updatedContextState = patientContextState
            .copyTyped()
            .also { it.bindingMdibVersion = bindingVersion }
            .also { it.descriptorVersion = badVersion }
            .also { it.stateVersion = badVersion }

        val stateModifications = MdibStateModifications.Context(
            listOf(updatedContextState.copyTyped(), updatedContextState)
        )

        var modifications = versionHandler.beforeFirstModification(stateModifications, mdibStorage)
        assertEquals(2, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(2, modifications.size)

        modifications.states.forEach {
            val state = assertInstanceOf(PatientContextState::class.java, it)

            assertEquals(Handles.CONTEXT_0, state.handle)
            assertEquals(BigInteger.ONE, state.stateVersion)
            assertEquals(patientEntity.descriptor.descriptorVersion, state.descriptorVersion)
            assertEquals(bindingVersion, state.bindingMdibVersion)
        }
    }

    /**
     * Inserts a single state descriptor and checks that the version is saved, then checks that starting a state
     * update flushes saved versions.
     */
    @Test
    fun `state update before flushes saved versions`() {
        baseStorage()

        val parentEntity = mdibStorage.getEntity(Handles.VMD_0).orElseThrow()
        val singleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, Pair.tryFromThrowing(parentEntity.descriptor, parentEntity.states[0]))
        val vmdPair = assertInstanceOf(SingleStatePair.Vmd::class.java, singleStatePair.pair)


        var modifications = listOf<MdibDescriptionModification>(
            MdibDescriptionModification.Insert(
                mockEntryFactory.entry(Handles.CHANNEL_3, ChannelDescriptor::class.java, ChannelState::class.java),
                Handles.VMD_0
            )
        )

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(2, modifications.size)

        val secondUpdate = assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[1])

        // make sure update bumps parent version
        run {
            val updateSingleStatePair = assertInstanceOf(Pair.SingleStatePair::class.java, secondUpdate.pair)
            val updatedVmd = assertInstanceOf(SingleStatePair.Vmd::class.java, updateSingleStatePair.pair)

            assertEquals(
                vmdPair.descriptor.descriptorVersion!!.inc(),
                updatedVmd.descriptor.descriptorVersion
            )

            assertEquals(
                vmdPair.descriptor.descriptorVersion!!.inc(),
                updatedVmd.state.descriptorVersion
            )

            assertEquals(
                vmdPair.state.stateVersion!!.inc(),
                updatedVmd.state.stateVersion
            )
        }

        // make sure update is in cache and removed after the before_first_modification call
        run {
            val interior = getInterior(versionHandler)

            assertNotNull(interior.getUpdatedVersions(Handles.VMD_0)) { "Handle not in cache" }

            versionHandler.beforeFirstModification(MdibStateModifications.Alert(emptyList()), mdibStorage)

            assertNull(interior.getUpdatedVersions(Handles.VMD_0)) { "Handle still in cache" }
        }
    }

    @Test
    fun `inserted context state uses correct descriptor version`() {
        baseStorage()

        // insert second patient context state
        val patientEntity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0).orElseThrow()
        val patientContextState = assertInstanceOf(PatientContextState::class.java, patientEntity.states[0])

        val newState = patientContextState
            .copyTyped()
            .also { it.handle = Handles.CONTEXT_6 }

        var stateModification: MdibStateModifications = MdibStateModifications.Context(listOf(newState))

        stateModification = versionHandler.beforeFirstModification(stateModification, mdibStorage)
        stateModification = versionHandler.process(stateModification, mdibStorage)
        stateModification = versionHandler.afterLastModification(stateModification, mdibStorage)
        assertEquals(1, stateModification.size)

        mdibStorage.apply(
            MdibVersion.increment(mdibStorage.mdibVersion),
            mdibStorage.mdStateVersion.inc(),
            stateModification
        )

        // update patient context descriptor to version 1
        var modifications = listOf<MdibDescriptionModification>(
            MdibDescriptionModification.Update(
                mockEntryFactory.contextEntry(
                    Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_0,
                    PatientContextDescriptor::class.java, PatientContextState::class.java
                )
            ),
        )

        modifications = versionHandler.beforeFirstModification(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        modifications = versionHandler.process(modifications, mdibStorage)
        assertEquals(1, modifications.size)

        run {
            val updateModification =
                assertInstanceOf(MdibDescriptionModification.Update::class.java, modifications[0])

            val updatePair = assertInstanceOf(
                MultiStatePair.PatientContext::class.java,
                assertInstanceOf(
                    Pair.MultiStatePair::class.java,
                    updateModification.pair
                ).pair
            )
            assertEquals(BigInteger.ONE, updatePair.descriptor.descriptorVersion)

            assertEquals(
                2,
                updatePair.state.size
            ) { "Second state update missing, required due to descriptor version change" }
            assertEquals(Handles.CONTEXT_6, updatePair.state[1].handle)

            updatePair.state.forEach {
                assertEquals(BigInteger.ONE, it.descriptorVersion)
                assertEquals(BigInteger.ONE, it.stateVersion)
            }
        }

        modifications = versionHandler.afterLastModification(modifications, mdibStorage)

        mdibStorage.apply(
            MdibVersion.increment(mdibStorage.mdibVersion),
            mdibStorage.mdDescriptionVersion.inc(),
            mdibStorage.mdStateVersion.inc(),
            MdibDescriptionModifications().also { it.addAll(modifications) }
        )

        // ensure that descriptor versions are 1 for descriptor and all states
        run {
            val storageEntity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0).orElseThrow()
            assertEquals(BigInteger.ONE, storageEntity.descriptor.descriptorVersion)

            listOf(Handles.CONTEXT_0, Handles.CONTEXT_6).forEach { stateHandle ->
                val state = mdibStorage.contextStates.find { it.handle == stateHandle }!!
                assertEquals(BigInteger.ONE, state.descriptorVersion)
            }
        }

        // add a new context state through a state update
        val secondNewState = assertInstanceOf(PatientContextState::class.java, mockEntryFactory.contextEntry(
            Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_7,
            PatientContextDescriptor::class.java, PatientContextState::class.java).states[0]
        )

        stateModification = MdibStateModifications.Context(listOf(secondNewState))
        stateModification = versionHandler.beforeFirstModification(stateModification, mdibStorage)
        stateModification = versionHandler.process(stateModification, mdibStorage)
        stateModification = versionHandler.afterLastModification(stateModification, mdibStorage)

        assertEquals(BigInteger.ONE, stateModification.states[0].descriptorVersion)
    }

    private fun incOptionalBigInteger(value: BigInteger?): BigInteger = value?.inc() ?: BigInteger.ONE

    private fun getInterior(versionHandler: VersionHandler): VersionHandlerInterior {
        val field = versionHandler.javaClass.getDeclaredField("interior")
        field.isAccessible = true
        return field.get(versionHandler) as VersionHandlerInterior
    }

    private fun baseStorage() {
        mdibStorage.apply(
            MdibVersion.increment(mdibStorage.mdibVersion),
            mdibStorage.mdDescriptionVersion.plus(BigInteger.ONE),
            mdibStorage.mdStateVersion.plus(BigInteger.ONE),
            BaseTreeModificationsSet(mockEntryFactory).createBaseTree()
        )
    }

}