package org.somda.sdc.biceps.provider.preprocessing

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.somda.sdc.biceps.UnitTestUtil
import org.somda.sdc.biceps.common.CommonConfig
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.MdibTypeValidator
import org.somda.sdc.biceps.common.storage.MdibStorage
import org.somda.sdc.biceps.common.storage.factory.MdibStorageFactory
import org.somda.sdc.biceps.guice.DefaultBicepsConfigModule
import org.somda.sdc.biceps.model.participant.MdibVersion
import org.somda.sdc.biceps.testutil.BaseTreeModificationsSet
import org.somda.sdc.biceps.testutil.MockEntryFactory
import test.org.somda.common.LoggingTestWatcher
import java.math.BigInteger

@ExtendWith(LoggingTestWatcher::class)
class EmptyWriteCheckerTest {
    private val unitTestUtil = UnitTestUtil(object : DefaultBicepsConfigModule() {
        override fun customConfigure() {
            // Configure to avoid copying and make comparison easier
            bind(
                CommonConfig.COPY_MDIB_OUTPUT,
                Boolean::class.java,
                false
            )
        }
    })

    private lateinit var mdibStorage: MdibStorage
    private lateinit var emptyWriteChecker: EmptyWriteChecker

    private val mdibTypeValidator = unitTestUtil.injector.getInstance(MdibTypeValidator::class.java)
    private val mockEntryFactory = MockEntryFactory(mdibTypeValidator)

    @BeforeEach
    fun beforeEach() {
        // Given a version handler and sample input
        mdibStorage = unitTestUtil.injector.getInstance(MdibStorageFactory::class.java)
            .createMdibStorage()
        emptyWriteChecker = unitTestUtil.injector.getInstance(EmptyWriteChecker::class.java)

        mdibStorage.apply(
            MdibVersion.create(),
            BigInteger.ZERO,
            BigInteger.ZERO,
            BaseTreeModificationsSet(mockEntryFactory).createBaseTree()
        )
    }

    @Test
    fun `empty descriptor write`() {
        var modifications = listOf<MdibDescriptionModification>()

        modifications = emptyWriteChecker.beforeFirstModification(modifications, mdibStorage)
        modifications = emptyWriteChecker.process(modifications, mdibStorage)
        Assertions.assertThrows(IllegalArgumentException::class.java) {
            emptyWriteChecker.afterLastModification(modifications, mdibStorage)
        }
    }

    @Test
    fun `empty state write`() {
        processState(MdibStateModifications.Alert(emptyList()), mdibStorage)
        processState(MdibStateModifications.Component(emptyList()), mdibStorage)
        processState(MdibStateModifications.Context(emptyList()), mdibStorage)
        processState(MdibStateModifications.Metric(emptyList()), mdibStorage)
        processState(MdibStateModifications.Operation(emptyList()), mdibStorage)
        processState(MdibStateModifications.Waveform(emptyList()), mdibStorage)
    }

    private fun processState(stateModifications: MdibStateModifications, mdibStorage: MdibStorage) {
        var modifications = stateModifications
        modifications = emptyWriteChecker.beforeFirstModification(modifications, mdibStorage)
        modifications = emptyWriteChecker.process(modifications, mdibStorage)
        Assertions.assertThrows(IllegalArgumentException::class.java) {
            emptyWriteChecker.afterLastModification(modifications, mdibStorage)
        }
    }

}