package org.somda.sdc.biceps.common.storage

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.somda.sdc.biceps.UnitTestUtil
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.storage.factory.MdibStoragePreprocessingChainFactory
import org.somda.sdc.biceps.model.participant.MdsDescriptor
import org.somda.sdc.biceps.model.participant.MdsState
import org.somda.sdc.biceps.model.participant.NumericMetricState
import org.somda.sdc.biceps.testutil.MockModelFactory
import test.org.somda.common.LoggingTestWatcher
import java.util.function.Consumer

@ExtendWith(LoggingTestWatcher::class)
class MdibStoragePreprocessingChainTestKt {

    private val unitTestUtil = UnitTestUtil()
    private var chainFactory: MdibStoragePreprocessingChainFactory = unitTestUtil.injector.getInstance(
        MdibStoragePreprocessingChainFactory::class.java
    )

    @Test
    fun `test description modification processing`() {
        // Given a preprocessing chain with 3 segments
        val mockStorage = Mockito.mock(MdibStorage::class.java)
        val segment1 = Mockito.mock(DescriptionPreprocessingSegment::class.java)
        val segment2 = Mockito.mock(DescriptionPreprocessingSegment::class.java)
        val segment3 = Mockito.mock(DescriptionPreprocessingSegment::class.java)
        val segments = listOf(segment1, segment2, segment3)
        val chain = chainFactory.createMdibStoragePreprocessingChain(
            mockStorage,
            segments,
            emptyList()
        )

        val expectedHandle = "foobarHandle"
        val modifications = MdibDescriptionModifications()
        modifications.insert(
            MockModelFactory.createPair(
                expectedHandle,
                MdsDescriptor::class.java,
                MdsState::class.java
            ), null
        )

        // make the mocks return the same list of modifications
        segments.forEach { segment ->
            Mockito.doReturn(modifications.asList())
                .`when`(segment)
                .beforeFirstModification(Mockito.eq(modifications.asList()), Mockito.any())

            Mockito.doReturn(modifications.asList())
                .`when`(segment)
                .process(Mockito.eq(modifications.asList()), Mockito.any())

            Mockito.doReturn(modifications.asList())
                .`when`(segment)
                .afterLastModification(Mockito.eq(modifications.asList()), Mockito.any())
        }

        run {
            // When there is a regular call to process description modifications
            chain.processDescriptionModifications(modifications)

            // Then expect every segment to be processed once
            for (segment in segments) {
                Mockito.verify(segment, Mockito.times(1))
                    .process(Mockito.eq(modifications.asList()), Mockito.same(mockStorage))
            }
        }

        run {
            // When there is a call that causes an exception during processing of segment2
            val expectedErrorMessage = "foobarMessage"
            Mockito.doThrow(Exception(expectedErrorMessage))
                .`when`(segment2)
                .process(modifications.asList(), mockStorage)

            // Then expect a PreprocessingException to be thrown
            val thrown = Assertions.assertThrows(PreprocessingException::class.java) {
                chain.processDescriptionModifications(modifications)
            }
            Assertions.assertEquals(expectedErrorMessage, thrown.message)
            Assertions.assertEquals(segment2.toString(), thrown.segment)

            // Then expect segment3 not to be processed
            Mockito.verify(segment3, Mockito.times(1)) // still one interaction only
                .process(modifications.asList(), mockStorage)
        }
    }

    @Test
    fun `test state modifications processing`() {
        // Given a preprocessing chain with 3 segments
        val mockStorage = Mockito.mock(MdibStorage::class.java)
        val segment1 = Mockito.mock(StatePreprocessingSegment::class.java)
        val segment2 = Mockito.mock(StatePreprocessingSegment::class.java)
        val segment3 = Mockito.mock(StatePreprocessingSegment::class.java)
        val segments = listOf(segment1, segment2, segment3)
        val chain = chainFactory.createMdibStoragePreprocessingChain(
            mockStorage,
            emptyList(),
            segments
        )

        val expectedHandle = "foobarHandle"
        val modifications: MdibStateModifications = MdibStateModifications.Metric(listOf(
                MockModelFactory.createState(
                    expectedHandle,
                    NumericMetricState::class.java
                )
            )
        )

        // make the mocks return the same modification
        segments.forEach { segment ->
            Mockito.doReturn(modifications)
                .`when`(segment)
                .beforeFirstModification(Mockito.eq(modifications), Mockito.any())

            Mockito.doReturn(modifications)
                .`when`(segment)
                .process(Mockito.eq(modifications), Mockito.any())

            Mockito.doReturn(modifications)
                .`when`(segment)
                .afterLastModification(Mockito.eq(modifications), Mockito.any())
        }

        run{
            // When there is a regular call to process state modifications
            chain.processStateModifications(modifications)

            // Then expect every segment to be processed once
            for (segment in segments) {
                Mockito.verify(segment, Mockito.times(1))
                    .process(modifications, mockStorage)
            }
        }

        run {
            // When there is a call that causes an exception during processing of segment2
            val expectedErrorMessage = "foobarMessage"
            Mockito.doThrow(Exception(expectedErrorMessage))
                .`when`(segment2)
                .process(modifications, mockStorage)

            // Then expect a PreprocessingException to be thrown
            val thrown = Assertions.assertThrows(PreprocessingException::class.java) {
                chain.processStateModifications(modifications)
            }
            Assertions.assertEquals(expectedErrorMessage, thrown.message)
            Assertions.assertEquals(segment2.toString(), thrown.segment)

            // Then expect segment3 not to be processed
            Mockito.verify(segment3, Mockito.times(1)) // still one interaction only
                .process(modifications, mockStorage)
        }
    }

    @Test
    fun `test description modifications pre- and postprocessing`() {
        // Given a preprocessing chain with 2 segments
        val mockStorage = Mockito.mock(MdibStorage::class.java)
        val segment1 = Mockito.mock(DescriptionPreprocessingSegment::class.java)
        val segment2 = Mockito.mock(DescriptionPreprocessingSegment::class.java)

        val segments = listOf(segment1, segment2)
        val chain = chainFactory.createMdibStoragePreprocessingChain(
            mockStorage,
            segments,
            emptyList()
        )

        // When there is a regular call to process state modifications
        val expectedModifications = MdibDescriptionModifications()

        // make the mocks return the same list of modifications
        segments.forEach { segment ->
            Mockito.doReturn(expectedModifications.asList())
                .`when`(segment)
                .beforeFirstModification(Mockito.eq(expectedModifications.asList()), Mockito.any())

            Mockito.doReturn(expectedModifications.asList())
                .`when`(segment)
                .process(Mockito.eq(expectedModifications.asList()), Mockito.any())

            Mockito.doReturn(expectedModifications.asList())
                .`when`(segment)
                .afterLastModification(Mockito.eq(expectedModifications.asList()), Mockito.any())
        }

        chain.processDescriptionModifications(expectedModifications)

        // Then expect before first and after last modification callbacks to be triggered for each segment
        segments.forEach { segment ->
            Mockito.verify(segment, Mockito.times(1))
                .beforeFirstModification(Mockito.eq(expectedModifications.asList()), Mockito.same(mockStorage))
            Mockito.verify(segment, Mockito.times(1))
                .afterLastModification(Mockito.eq(expectedModifications.asList()), Mockito.same(mockStorage))
        }
    }

    @Test
    fun `test state modification pre- and postprocessing`() {
        // Given a preprocessing chain with 2 segments
        val mockStorage = Mockito.mock(MdibStorage::class.java)
        val segment1 = Mockito.mock(StatePreprocessingSegment::class.java)
        val segment2 = Mockito.mock(StatePreprocessingSegment::class.java)

        val segments = listOf(segment1, segment2)
        val chain = chainFactory.createMdibStoragePreprocessingChain(
            mockStorage,
            listOf(),
            segments
        )

        // When there is a regular call to process state modifications
        val metricModification = MdibStateModifications.Metric(emptyList())

        // make the mocks return the same modification
        segments.forEach { segment ->
            Mockito.doReturn(metricModification)
                .`when`(segment)
                .beforeFirstModification(Mockito.eq(metricModification), Mockito.any())

            Mockito.doReturn(metricModification)
                .`when`(segment)
                .process(Mockito.eq(metricModification), Mockito.any())

            Mockito.doReturn(metricModification)
                .`when`(segment)
                .afterLastModification(Mockito.eq(metricModification), Mockito.any())
        }

        chain.processStateModifications(metricModification)

        // Then expect before first and after last modification callbacks to be triggered for each segment
        segments.forEach(Consumer { segment: StatePreprocessingSegment ->
            Mockito.verify(segment, Mockito.times(1))
                .beforeFirstModification(metricModification, mockStorage)
            Mockito.verify(segment, Mockito.times(1))
                .afterLastModification(metricModification, mockStorage)
        })
    }
}