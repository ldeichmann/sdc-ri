package org.somda.sdc.biceps.common

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.somda.sdc.biceps.model.participant.MdsDescriptor
import org.somda.sdc.biceps.model.participant.MdsState
import org.somda.sdc.biceps.model.participant.NumericMetricDescriptor
import org.somda.sdc.biceps.model.participant.NumericMetricState
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor
import org.somda.sdc.biceps.model.participant.PatientContextState
import org.somda.sdc.biceps.testutil.MockModelFactory
import java.lang.RuntimeException

class MdibDescriptionModificationsTest {
    @Test
    fun `duplicate single state`() {
        val modification = MdibDescriptionModifications()

        val handles = listOf("h1", "h2", "h3")

        modification
            .insert(
                MockModelFactory.createPair(
                    handles[0],
                    NumericMetricDescriptor::class.java,
                    NumericMetricState::class.java
                ), null
            )
            .insert(
                MockModelFactory.createPair(
                    handles[1],
                    NumericMetricDescriptor::class.java,
                    NumericMetricState::class.java
                ), null
            )
            .insert(
                MockModelFactory.createPair(
                    handles[2],
                    NumericMetricDescriptor::class.java,
                    NumericMetricState::class.java
                ), null
            )

        handles.forEach { handle ->
            Assertions.assertNotNull(modification.asList().find { it.descriptorHandle == handle })
        }

        run {
            val thrown = Assertions.assertThrows(IllegalArgumentException::class.java) {
                modification.insert(
                    MockModelFactory.createPair(handles[0], MdsDescriptor::class.java, MdsState::class.java),
                    null
                )
            }
            Assertions.assertTrue(thrown.message?.contains(handles[0]) ?: false)
        }

        run {
            val thrown = Assertions.assertThrows(IllegalArgumentException::class.java) {
                modification.update(
                    MockModelFactory.createPair(
                        handles[0],
                        MdsDescriptor::class.java,
                        MdsState::class.java
                    )
                )
            }
            Assertions.assertTrue(thrown.message?.contains(handles[0]) ?: false)
        }

        run {
            val thrown = Assertions.assertThrows(
                IllegalArgumentException::class.java
            ) {
                modification.delete(handles[0])
            }
            Assertions.assertTrue(thrown.message?.contains(handles[0]) ?: false)
        }
    }

    @Test
    fun `duplicate multi state handle`() {
        val modification = MdibDescriptionModifications()

        val handles = listOf("h1", "h2", "h3")

        modification
            .insert(
                MockModelFactory.createPair(
                    handles[0],
                    NumericMetricDescriptor::class.java,
                    NumericMetricState::class.java
                ), null
            )
            .insert(
                MockModelFactory.createPair(
                    handles[1],
                    NumericMetricDescriptor::class.java,
                    NumericMetricState::class.java
                ), null
            )
            .insert(
                MockModelFactory.createPair(
                    handles[2],
                    NumericMetricDescriptor::class.java,
                    NumericMetricState::class.java
                ), null
            )

        // descriptor collides
        run {
            val thrown = Assertions.assertThrows(IllegalArgumentException::class.java) {
                modification.insert(
                    MockModelFactory.createContextStatePair(
                        handles[0],
                        null,
                        PatientContextDescriptor::class.java,
                        PatientContextState::class.java,
                        "other"
                    ),
                    null
                )
            }
            Assertions.assertTrue(thrown.message?.contains(handles[0]) ?: false)
        }

        run {
            val thrown = Assertions.assertThrows(IllegalArgumentException::class.java) {
                modification.delete(handles[0])
            }
            Assertions.assertTrue(thrown.message?.contains(handles[0]) ?: false)
        }

        // state collides
        run {
            val thrown = Assertions.assertThrows(IllegalArgumentException::class.java) {
                modification.insert(
                    MockModelFactory.createContextStatePair(
                        "other",
                        null,
                        PatientContextDescriptor::class.java,
                        PatientContextState::class.java,
                        handles[0]
                    ),
                    null
                )
            }
            Assertions.assertTrue(thrown.message?.contains(handles[0]) ?: false)
        }
    }

    @Test
    fun `duplicate in addAll`() {
        val modification = MdibDescriptionModifications()

        val handles = listOf("h1", "h2", "h3")

        // descriptors collide
        run {
            val thrown = Assertions.assertThrows(RuntimeException::class.java) {
                modification.addAll(
                    listOf(
                        MdibDescriptionModification.Insert(
                            MockModelFactory.createPair(
                                handles[1],
                                NumericMetricDescriptor::class.java,
                                NumericMetricState::class.java
                            ), null
                        ),
                        MdibDescriptionModification.Update(
                            MockModelFactory.createPair(
                                handles[1],
                                NumericMetricDescriptor::class.java,
                                NumericMetricState::class.java
                            )
                        ),
                    )
                )
            }

            Assertions.assertTrue(thrown.message?.contains(handles[1]) ?: false)
            Assertions.assertTrue(modification.asList().isEmpty()) { "Partially added modifications is bad" }
        }

        // descriptor and state
        run {
            val thrown = Assertions.assertThrows(RuntimeException::class.java) {
                modification.addAll(
                    listOf(
                        MdibDescriptionModification.Insert(
                            MockModelFactory.createPair(
                                handles[1],
                                NumericMetricDescriptor::class.java,
                                NumericMetricState::class.java
                            ), null
                        ),
                        MdibDescriptionModification.Update(
                            MockModelFactory.createContextStatePair(
                                "other",
                                null,
                                PatientContextDescriptor::class.java,
                                PatientContextState::class.java,
                                handles[1]
                            ),
                        ),
                    )
                )
            }

            Assertions.assertTrue(thrown.message?.contains(handles[1]) ?: false)
            Assertions.assertTrue(modification.asList().isEmpty()) { "Partially added modifications is bad" }
        }

    }
}